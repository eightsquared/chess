# chess

A simple, efficient [Go](https://golang.org) library for describing, validating, and serializing chess positions and moves.

## Documentation

[![API Reference](https://godoc.org/gitlab.com/eightsquared/chess?status.svg)](https://godoc.org/gitlab.com/eightsquared/chess)

## Installation

    go get -u gitlab.com/eightsquared/chess

## License

[MIT](LICENSE)
