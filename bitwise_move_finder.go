package chess

// BitwiseMoveFinder provides an optimized move detection algorithm that uses
// bitmasks and other tricks to improve the performance of move and attack
// detection logic. Benchmarking shows that this algorithm runs approximately
// 40% faster than and with less than half the allocations per call of the
// standard move finder.
type BitwiseMoveFinder struct {
}

// Moves determines the set of valid moves ms according to the current state of
// the given Game g.
func (bmf *BitwiseMoveFinder) Moves(g Game) (ms Moves) {
	b := g.Board

	// Assign the color with the move to att(acking), and the opponent's color to
	// def(ending).
	att, def := g.ToMove, g.ToMove.Invert()

	// Set aside a four-element array as "scratch space" for building potential
	// moves. Only in the event a move is found to be valid will we pay to
	// allocate a new Move slice to hold it.
	m := make(Move, 4)
	var mlen int

	// Here we loop through all pieces to compose several bitmaps that will allow
	// for simple and inexpensive move checks using bitwise operations. Each bit
	// in these maps corresponds to a square, with a8 being the most significant
	// bit and h1 being the least significant. (The mapping from squares to bits
	// is kept in the package global squareBits variable.) attMap and defMap will
	// record the positions of all attacking and defending pieces, respectively.
	// defAttackMap will hold the union of all squares attacked by defending
	// pieces if each piece is considered in isolation. (This map provides an
	// opportunity for an early exit from the chekAndAppend function below.) We
	// also capture the attacking king's square in ks while looping.
	var attMap, defMap, defAttackMap uint64
	ks := None
	for s := First; s <= Last; s++ {
		piece, bit := b[s], squareBits[s]
		switch {
		case piece.Is(att):
			attMap |= bit
			if piece.Is(King) {
				ks = s
			}
		case piece.Is(def):
			defMap |= bit
			switch {
			case piece.Is(WhitePawn):
				defAttackMap |= whitePawnCapturePatterns[s]
			case piece.Is(BlackPawn):
				defAttackMap |= blackPawnCapturePatterns[s]
			case piece.Is(Knight):
				defAttackMap |= knightMovePatterns[s]
			case piece.Is(Bishop):
				defAttackMap |= bishopMovePatterns[s]
			case piece.Is(Rook):
				defAttackMap |= rookMovePatterns[s]
			case piece.Is(Queen):
				defAttackMap |= bishopMovePatterns[s]
				defAttackMap |= rookMovePatterns[s]
			case piece.Is(King):
				defAttackMap |= kingMovePatterns[s]
			}
		}
	}
	// With all piece positions captured, we can invert the union of these two
	// maps to get the map of empty squares.
	emptyMap := ^(attMap | defMap)

	// epMap will have exactly one bit turned on if the current en passant target
	// square (aliased to eps) is not None.
	var epMap uint64
	eps := g.EnPassant
	if eps != None {
		epMap = squareBits[eps]
	}

	// checkAndAppend will append the current move (in the scratch space m) to ms
	// if and only if applying the move to the current board does not result in
	// the given square s being placed under attack by the defending color. The
	// return value indicates whether the move was appended.
	checkAndAppend := func(s Square) (ok bool) {
		b2 := b.Apply(m[:mlen])
		if s != None &&
			defAttackMap&squareBits[s] != 0 &&
			bmf.Attacked(b2, s, def) {
			return
		}
		n := make(Move, mlen)
		copy(n, m)
		ms, ok = append(ms, n), true
		return
	}

	// The following loop follows the same basic pattern as the standard move
	// finder with the exception that this algorithm relies on intersecting
	// precomputed move pattern bitmaps with the current board state bitmaps to
	// determine whether it is possible for a piece to move to a particular
	// square.
	var bit uint64
	for s := First; s <= Last; s++ {
		bit = squareBits[s]
		if attMap&bit == 0 {
			continue
		}
		p := b[s]
		if p.Is(Pawn) {
			var moveMap uint64
			var captureMap uint64
			var promotionRank rune
			if p.Is(White) {
				moveMap = whitePawnMovePatterns[s] & emptyMap
				captureMap = whitePawnCapturePatterns[s] & (defMap | epMap)
				promotionRank = '8'
			} else {
				moveMap = blackPawnMovePatterns[s] & emptyMap
				captureMap = blackPawnCapturePatterns[s] & (defMap | epMap)
				promotionRank = '1'
			}
			// Advance
			if moveMap != 0 {
				for a := s.Forward(p); a != None && moveMap&squareBits[a] != 0; a = a.Forward(p) {
					if a.Rank() == promotionRank {
						m[0], m[1], m[2], m[3], mlen = s, a, ReplaceWithKnight, a, 4
						if checkAndAppend(ks) {
							ms = append(ms, Move{s, a, ReplaceWithBishop, a})
							ms = append(ms, Move{s, a, ReplaceWithRook, a})
							ms = append(ms, Move{s, a, ReplaceWithQueen, a})
						}
					} else {
						m[0], m[1], mlen = s, a, 2
						checkAndAppend(ks)
					}
				}
			}
			// Capture
			if captureMap != 0 {
				// Capture right
				if a := s.Right().Forward(p); a != None && captureMap&squareBits[a] != 0 {
					if a == eps {
						m[0], m[1], m[2], m[3], mlen = s, a, s.Right(), None, 4
						checkAndAppend(ks)
					} else if a.Rank() == promotionRank {
						m[0], m[1], m[2], m[3], mlen = s, a, ReplaceWithKnight, a, 4
						if checkAndAppend(ks) {
							ms = append(ms, Move{s, a, ReplaceWithBishop, a})
							ms = append(ms, Move{s, a, ReplaceWithRook, a})
							ms = append(ms, Move{s, a, ReplaceWithQueen, a})
						}
					} else {
						m[0], m[1], mlen = s, a, 2
						checkAndAppend(ks)
					}
				}
				// Capture left
				if a := s.Left().Forward(p); a != None && captureMap&squareBits[a] != 0 {
					if a == eps {
						m[0], m[1], m[2], m[3], mlen = s, a, s.Left(), None, 4
						checkAndAppend(ks)
					} else if a.Rank() == promotionRank {
						m[0], m[1], m[2], m[3], mlen = s, a, ReplaceWithKnight, a, 4
						if checkAndAppend(ks) {
							ms = append(ms, Move{s, a, ReplaceWithBishop, a})
							ms = append(ms, Move{s, a, ReplaceWithRook, a})
							ms = append(ms, Move{s, a, ReplaceWithQueen, a})
						}
					} else {
						m[0], m[1], mlen = s, a, 2
						checkAndAppend(ks)
					}
				}
			}
		}
		if p.Is(Knight) {
			if moveMap := knightMovePatterns[s] & (emptyMap | defMap); moveMap != 0 {
				if b := bit << 17; b != 0 && moveMap&b != 0 {
					m[0], m[1], mlen = s, s.Add(Offset{-1, 2}), 2
					checkAndAppend(ks)
				}
				if b := bit << 15; b != 0 && moveMap&b != 0 {
					m[0], m[1], mlen = s, s.Add(Offset{1, 2}), 2
					checkAndAppend(ks)
				}
				if b := bit << 10; b != 0 && moveMap&b != 0 {
					m[0], m[1], mlen = s, s.Add(Offset{-2, 1}), 2
					checkAndAppend(ks)
				}
				if b := bit << 6; b != 0 && moveMap&b != 0 {
					m[0], m[1], mlen = s, s.Add(Offset{2, 1}), 2
					checkAndAppend(ks)
				}
				if b := bit >> 6; b != 0 && moveMap&b != 0 {
					m[0], m[1], mlen = s, s.Add(Offset{-2, -1}), 2
					checkAndAppend(ks)
				}
				if b := bit >> 10; b != 0 && moveMap&b != 0 {
					m[0], m[1], mlen = s, s.Add(Offset{2, -1}), 2
					checkAndAppend(ks)
				}
				if b := bit >> 15; b != 0 && moveMap&b != 0 {
					m[0], m[1], mlen = s, s.Add(Offset{-1, -2}), 2
					checkAndAppend(ks)
				}
				if b := bit >> 17; b != 0 && moveMap&b != 0 {
					m[0], m[1], mlen = s, s.Add(Offset{1, -2}), 2
					checkAndAppend(ks)
				}
			}
		}
		if p.Is(Bishop) || p.Is(Queen) {
			if moveMap := bishopMovePatterns[s] & (emptyMap | defMap); moveMap != 0 {
				// Northwest
				a := s
				o := Offset{-1, 1}
				for b := bit << 9; moveMap&b != 0 && a.File() != 'a' && a.Rank() != '8'; b <<= 9 {
					a = a.Add(o)
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(ks)
					if defMap&b != 0 {
						break
					}
				}
				// Northeast
				a = s
				o.Files, o.Ranks = 1, 1
				for b := bit << 7; moveMap&b != 0 && a.File() != 'h' && a.Rank() != '8'; b <<= 7 {
					a = a.Add(o)
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(ks)
					if defMap&b != 0 {
						break
					}
				}
				// Southwest
				a = s
				o.Files, o.Ranks = -1, -1
				for b := bit >> 7; moveMap&b != 0 && a.File() != 'a' && a.Rank() != '1'; b >>= 7 {
					a = a.Add(o)
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(ks)
					if defMap&b != 0 {
						break
					}
				}
				// Southeast
				a = s
				o.Files, o.Ranks = 1, -1
				for b := bit >> 9; moveMap&b != 0 && a.File() != 'h' && a.Rank() != '1'; b >>= 9 {
					a = a.Add(o)
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(ks)
					if defMap&b != 0 {
						break
					}
				}
			}
		}
		if p.Is(Rook) || p.Is(Queen) {
			if moveMap := rookMovePatterns[s] & (emptyMap | defMap); moveMap != 0 {
				// North
				a := s
				for b := bit << 8; moveMap&b != 0 && a.Rank() != '8'; b <<= 8 {
					a = a.Up()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(ks)
					if defMap&b != 0 {
						break
					}
				}
				// West
				a = s
				for b := bit << 1; moveMap&b != 0 && a.File() != 'a'; b <<= 1 {
					a = a.Left()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(ks)
					if defMap&b != 0 {
						break
					}
				}
				// East
				a = s
				for b := bit >> 1; moveMap&b != 0 && a.File() != 'h'; b >>= 1 {
					a = a.Right()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(ks)
					if defMap&b != 0 {
						break
					}
				}
				// South
				a = s
				for b := bit >> 8; moveMap&b != 0 && a.Rank() != '1'; b >>= 8 {
					a = a.Down()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(ks)
					if defMap&b != 0 {
						break
					}
				}
			}
		}
		if p.Is(King) {
			if moveMap := kingMovePatterns[s] & (emptyMap | defMap); moveMap != 0 {
				// Northwest
				if b := bit << 9; b != 0 && moveMap&b != 0 {
					a := s.Up().Left()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(a)
				}
				// North
				if b := bit << 8; b != 0 && moveMap&b != 0 {
					a := s.Up()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(a)
				}
				// Northeast
				if b := bit << 7; b != 0 && moveMap&b != 0 {
					a := s.Up().Right()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(a)
				}
				// West
				if b := bit << 1; b != 0 && moveMap&b != 0 {
					a := s.Left()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(a)
				}
				// East
				if b := bit >> 1; b != 0 && moveMap&b != 0 {
					a := s.Right()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(a)
				}
				// Southwest
				if b := bit >> 7; b != 0 && moveMap&b != 0 {
					a := s.Down().Left()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(a)
				}
				// South
				if b := bit >> 8; b != 0 && moveMap&b != 0 {
					a := s.Down()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(a)
				}
				// Southeast
				if b := bit >> 9; b != 0 && moveMap&b != 0 {
					a := s.Down().Right()
					m[0], m[1], mlen = s, a, 2
					checkAndAppend(a)
				}
				// Castling (white)
				if att.Is(White) && s == E1 {
					if g.Castling.Allows(WhiteKingside) &&
						b[F1] == Empty && b[G1] == Empty &&
						b[H1] == WhiteRook &&
						!bmf.Attacked(b, E1, def) {
						m[0], m[1], m[2], m[3], mlen = s, G1, H1, F1, 4
						checkAndAppend(G1)
					}
					if g.Castling.Allows(WhiteQueenside) &&
						b[D1] == Empty && b[C1] == Empty && b[B1] == Empty &&
						b[A1] == WhiteRook &&
						!bmf.Attacked(b, E1, def) &&
						!bmf.Attacked(b, D1, def) {
						m[0], m[1], m[2], m[3], mlen = s, C1, A1, D1, 4
						checkAndAppend(C1)
					}
				}
				// Castling (black)
				if att.Is(Black) && s == E8 {
					if g.Castling.Allows(BlackKingside) &&
						b[F8] == Empty && b[G8] == Empty &&
						b[H8] == BlackRook &&
						!bmf.Attacked(b, E8, def) {
						m[0], m[1], m[2], m[3], mlen = s, G8, H8, F8, 4
						checkAndAppend(G8)
					}
					if g.Castling.Allows(BlackQueenside) &&
						b[D8] == Empty && b[C8] == Empty && b[B8] == Empty &&
						b[A8] == BlackRook &&
						!bmf.Attacked(b, E8, def) &&
						!bmf.Attacked(b, D8, def) {
						m[0], m[1], m[2], m[3], mlen = s, C8, A8, D8, 4
						checkAndAppend(C8)
					}
				}
			}
		}
	}
	return
}

// Attacked returns true if the given square s in the given board b is attacked
// by any piece of the same color as the given piece p.
func (*BitwiseMoveFinder) Attacked(b Board, s Square, p Piece) (attacked bool) {
	if !s.Valid() {
		return false
	}
	c, bit := p.Color(), squareBits[s]
	for d := First; d <= Last; d++ {
		if !b[d].Is(c) {
			continue
		}
		switch {
		case b[d].Is(WhitePawn):
			if whitePawnCapturePatterns[d]&bit != 0 {
				return true
			}
		case b[d].Is(BlackPawn):
			if blackPawnCapturePatterns[d]&bit != 0 {
				return true
			}
		case b[d].Is(Knight):
			if knightMovePatterns[d]&bit != 0 {
				return true
			}
		case b[d].Is(King):
			if kingMovePatterns[d]&bit != 0 {
				return true
			}
		default:
			if ((b[d].Is(Bishop) || b[d].Is(Queen)) && bishopMovePatterns[d]&bit != 0) ||
				((b[d].Is(Rook) || b[d].Is(Queen)) && rookMovePatterns[d]&bit != 0) {
				u := s.Sub(d).Unit()
				for a := d.Add(u); a != None; a = a.Add(u) {
					if a == s {
						return true
					}
					if b[a] != Empty {
						break
					}
				}
			}
		}
	}
	return false
}

func (*BitwiseMoveFinder) String() string {
	return "bitwise"
}
