package chess

import (
	"math/rand"
)

// Board represents the current state of a chess board, where each element in
// the array represents a particular square. By convention, the squares are
// listed from top left to bottom right according to white's point of view, so
// that the first element corresponds to a8 and the last element corresponds to
// h1.
type Board [64]Piece

// DefaultInitialBoard describes the standard starting positions of all pieces.
var DefaultInitialBoard = Board{
	A8: BlackRook,
	B8: BlackKnight,
	C8: BlackBishop,
	D8: BlackQueen,
	E8: BlackKing,
	F8: BlackBishop,
	G8: BlackKnight,
	H8: BlackRook,
	A7: BlackPawn,
	B7: BlackPawn,
	C7: BlackPawn,
	D7: BlackPawn,
	E7: BlackPawn,
	F7: BlackPawn,
	G7: BlackPawn,
	H7: BlackPawn,
	A2: WhitePawn,
	B2: WhitePawn,
	C2: WhitePawn,
	D2: WhitePawn,
	E2: WhitePawn,
	F2: WhitePawn,
	G2: WhitePawn,
	H2: WhitePawn,
	A1: WhiteRook,
	B1: WhiteKnight,
	C1: WhiteBishop,
	D1: WhiteQueen,
	E1: WhiteKing,
	F1: WhiteBishop,
	G1: WhiteKnight,
	H1: WhiteRook,
}

// Apply returns the state of the board after the given move has been applied.
// The squares in the move are evaluated in pairs of squares (s, t) using the
// following rules:
//
//	s and t are valid squares: the piece at s is moved to t.
//	s is valid, but t is not: the piece at s is removed.
//	s is not valid, but t is: the piece at t is replaced according to s.
//
// If the given move has fewer than two squares, the returned board will be an
// exact copy. If the given move has an odd number of squares, the last square
// will be ignored.
func (b Board) Apply(m Move) (c Board) {
	c = b
	for i := 1; i < len(m); i += 2 {
		s, t := m[i-1], m[i]
		if s.Valid() && t.Valid() {
			c[s], c[t] = Empty, c[s]
		} else if s.Valid() && !t.Valid() {
			c[s] = Empty
		} else if !s.Valid() && t.Valid() {
			c[t] = s.Replacement(c[t])
		}
	}
	return
}

// Equals determines whether board b is exactly the same as board c.
func (b Board) Equals(c Board) (eq bool) {
	for i := range b {
		if b[i] != c[i] {
			return
		}
	}
	eq = true
	return
}

// Find locates the first square t occurring after square s that contains a
// piece p. To start searching from the beginning of the board (a8), provide
// None for s. Find returns None if it does not find a matching piece.
//
// By calling this method repeatedly, you can loop through all pieces of a
// particular type:
//
//	for s := b.Find(WhitePawn, None); s != None; s = b.Find(WhitePawn, s) {
//		// b[s] == WhitePawn
//	}
//
// For flexibility, pieces are compared with the Piece.Is method rather than
// with strict equality. This allows you to search for all white pieces
// regardless of class or all bishops regardless of color.
func (b Board) Find(p Piece, s Square) (t Square) {
	t = None
	if s < First {
		s = First - 1
	}
	for s = s + 1; s <= Last; s++ {
		if b[s].Is(p) {
			t = s
			return
		}
	}
	return
}

// Shuffle returns a shuffled version of the board using the default random
// number generator. Because pieces are only swapped, the shuffled board will
// always have the same set of pieces as the original board. No consideration
// is given to ensuring that the shuffled board represents a potentially valid
// position—pawns may end up on the first or last ranks, or both kings may be
// in check simultaneously.
//
// This method has no practical use in real-life scenarios, but it is
// convenient for quickly generating random boards for large-scale fuzz
// testing. (This function is exported precisely so that external packages can
// take advantage of it in tests.)
func (b Board) Shuffle() (c Board) {
	c = b
	for i := len(b) - 1; i > 0; i-- {
		j := rand.Intn(i + 1)
		c[i], c[j] = c[j], c[i]
	}
	return
}

// String produces a rudimentary string representation of a board, where each
// rank is formatted as an eight-character line containing one character per
// square. All lines are terminated with a single newline character.
func (b Board) String() (s string) {
	p := make([]byte, 72)
	j := 0
	for i := 0; i < 64; i++ {
		if i > 0 && i%8 == 0 {
			p[j] = '\n'
			j++
		}
		p[j] = byte(b[i].Rune())
		j++
	}
	p[j] = '\n'
	s = string(p)
	return
}
