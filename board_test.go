package chess

import (
	"fmt"
	"testing"
)

func TestBoardApply(t *testing.T) {
	t.Parallel()
	initial := Board{D4: WhitePawn}
	cases := []struct {
		title    string
		move     Move
		expected Board
	}{
		{"an empty move is ignored", Move{}, Board{D4: WhitePawn}},
		{"a move with one square is ignored", Move{D4}, Board{D4: WhitePawn}},
		{"a move with two valid squares moves the piece from the first square to the second", Move{D4, D5}, Board{D5: WhitePawn}},
		{"a move with a valid square and an invalid square removes the piece from the first square", Move{D4, None}, Board{}},
		{"a move with a replacement square and a valid square changes the piece on the second square", Move{ReplaceWithKnight, D4}, Board{D4: WhiteKnight}},
		{"a move with multiple valid squares moves pieces multiple times", Move{D4, D5, D5, D6}, Board{D6: WhitePawn}},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			actual := initial.Apply(cases[i].move)
			if !actual.Equals(cases[i].expected) {
				t.Errorf("expected: %s; received: %s", cases[i].expected, actual)
			}
		}
	}
	for i, c := range cases {
		t.Run(c.title, test(i))
	}
}

func TestBoardEquals(t *testing.T) {
	t.Parallel()
	cases := []struct {
		a        Board
		b        Board
		expected bool
	}{
		{Board{}, Board{}, true},
		{Board{A1: WhiteQueen}, Board{}, false},
		{Board{A1: WhiteQueen}, Board{H8: WhiteQueen}, false},
		{Board{A1: WhiteQueen}, Board{A1: WhiteQueen, A2: WhiteRook}, false},
		{Board{A1: WhiteQueen}, Board{A1: WhiteQueen}, true},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			actual, expected := cases[i].a.Equals(cases[i].b), cases[i].expected
			if actual != expected {
				t.Errorf("expected: %v; received: %v", expected, actual)
			}
		}
	}
	for i := range cases {
		t.Run(fmt.Sprintf("%d", i), test(i))
	}
}

func TestBoardFind(t *testing.T) {
	t.Parallel()
	board := Board{
		A8: BlackRook,
		H8: BlackRook,
		B7: BlackKnight,
		G7: BlackBishop,
		C6: BlackKing,
		F6: BlackQueen,
		D5: BlackPawn,
		E5: BlackPawn,
		D4: WhitePawn,
		E4: WhitePawn,
		C3: WhiteKing,
		F3: WhiteQueen,
		B2: WhiteKnight,
		G2: WhiteBishop,
		A1: WhiteRook,
		H1: WhiteRook,
	}
	cases := []struct {
		piece    Piece
		after    Square
		expected Square
	}{
		{Pawn, None, D5},
		{Pawn, D5, E5},
		{Pawn, E5, D4},
		{Pawn, D4, E4},
		{Pawn, E4, None},
		{Knight, None, B7},
		{Knight, B7, B2},
		{Knight, B2, None},
		{Bishop, None, G7},
		{Bishop, G7, G2},
		{Bishop, G2, None},
		{Rook, None, A8},
		{Rook, A8, H8},
		{Rook, H8, A1},
		{Rook, A1, H1},
		{Rook, H1, None},
		{Queen, None, F6},
		{Queen, F6, F3},
		{Queen, F3, None},
		{King, None, C6},
		{King, C6, C3},
		{King, C3, None},
		{WhitePawn, None, D4},
		{WhitePawn, D4, E4},
		{WhitePawn, E4, None},
		{WhiteKnight, None, B2},
		{WhiteKnight, B2, None},
		{WhiteBishop, None, G2},
		{WhiteBishop, G2, None},
		{WhiteRook, None, A1},
		{WhiteRook, A1, H1},
		{WhiteRook, H1, None},
		{WhiteQueen, None, F3},
		{WhiteQueen, F3, None},
		{WhiteKing, None, C3},
		{WhiteKing, C3, None},
		{BlackPawn, None, D5},
		{BlackPawn, D5, E5},
		{BlackPawn, E5, None},
		{BlackKnight, None, B7},
		{BlackKnight, B7, None},
		{BlackBishop, None, G7},
		{BlackBishop, G7, None},
		{BlackRook, None, A8},
		{BlackRook, A8, H8},
		{BlackRook, H8, None},
		{BlackQueen, None, F6},
		{BlackQueen, F6, None},
		{BlackKing, None, C6},
		{BlackKing, C6, None},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			actual, expected := board.Find(cases[i].piece, cases[i].after), cases[i].expected
			if actual != expected {
				t.Errorf("expected: %s; received: %s", expected, actual)
			}
		}
	}
	for i, c := range cases {
		t.Run(fmt.Sprintf("find %c after %s", c.piece.Rune(), c.after), test(i))
	}
}

func TestBoardString(t *testing.T) {
	t.Parallel()
	board := Board{
		A1: WhiteRook,
		B2: WhiteKnight,
		C3: WhiteKing,
		D4: WhitePawn,
		E4: WhitePawn,
		F3: WhiteQueen,
		G2: WhiteBishop,
		H1: WhiteRook,
		A8: BlackRook,
		B7: BlackKnight,
		C6: BlackKing,
		D5: BlackPawn,
		E5: BlackPawn,
		F6: BlackQueen,
		G7: BlackBishop,
		H8: BlackRook,
	}
	expected := "r      r\n n    b \n  k  q  \n   pp   \n   PP   \n  K  Q  \n N    B \nR      R\n"
	if actual := board.String(); actual != expected {
		t.Errorf("board string: expected %q; received %q", expected, actual)
	}
}
