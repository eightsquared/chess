package chess

// CastlingRights describes which castling rights remain for both white and
// black.
type CastlingRights uint8

// These constants define the flags and combinations of flags used to describe
// castling rights.
const (
	WhiteKingside       CastlingRights = 1
	WhiteQueenside      CastlingRights = 2
	BlackKingside       CastlingRights = 4
	BlackQueenside      CastlingRights = 8
	WhiteBothSides      CastlingRights = WhiteKingside | WhiteQueenside
	BlackBothSides      CastlingRights = BlackKingside | BlackQueenside
	BothColorsBothSides CastlingRights = WhiteBothSides | BlackBothSides
)

// Allows determines whether the castling rights cr include all the rights
// denoted by the given rights r.
func (cr CastlingRights) Allows(r CastlingRights) bool {
	return cr&r == r
}

// AllowsAny determines whether the castling rights cr include any of the
// rights denoted by the given rights r.
func (cr CastlingRights) AllowsAny(r CastlingRights) bool {
	return cr&r != 0
}

// Grant returns a copy of the castling rights cr with the given rights r
// added.
func (cr CastlingRights) Grant(r CastlingRights) CastlingRights {
	return cr | r
}

// Revoke returns a copy of the castling rights cr with the given rights r
// removed.
func (cr CastlingRights) Revoke(r CastlingRights) CastlingRights {
	return cr & ^r
}
