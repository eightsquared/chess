/*
Package chess provides a simple library for describing and manipulating chess
positions and determining the set of valid moves for a given position.

Currently this library only supports the standard chess rules as accepted by
FIDE. There are no plans at the moment to expand support to the many chess
variants, but it may be possible to support variants such as Chess960 with
relatively few modifications.

Efficiency is a particular emphasis of this library. On reasonably recent
hardware, determining the set of valid moves for a given position should take
on the order of 10 microseconds, while formatting this set of moves in Standard
Algebraic Notation should take even less time. Optimizations are generally
accompanied by extensive comments; you are encouraged to read the code and
contribute changes to improve performance even further.

Games

To describe a game, create a Game struct. A Game should contain enough
information beyond the state of the board to determine the set of valid moves.
You can quickly create a game with the standard starting position:

	g := chess.Game{
		Board:    chess.DefaultInitialBoard,
		ToMove:   chess.White,
		Castling: chess.BothColorsBothSides,
	}

You may also use the square and piece constants defined in this package to
naturally describe an arbitrary board:

	// White to move and mate in one.
	g := chess.Game{
		Board:  chess.Board{
			chess.D8: chess.BlackKing,
			chess.B7: chess.WhiteQueen,
			chess.C6: chess.WhiteKing,
		},
		ToMove: chess.White,
	}

The fen subpackage (http://godoc.org/gitlab.com/eightsquared/chess/fen)
provides support for parsing and formatting game states with Forsyth–Edwards
Notation.

Moves

To find the set of moves available to the player with the move, create a move
finder. This package provides two such move finders that are covered by the
same test suite and additionally tested against each other. The bitwise move
finder generally yields the best performance.

	mf := new(chess.BitwiseMoveFinder)
	ms := mf.Moves(g)

The move finder returns a slice of moves in a generic format. (Each move is
represented as a slice of squares.) You may wish to format these moves in
Standard Algebraic Notation (SAN) for display to the user. To accomplish this,
use the Format method defined on the Moves type:

	san := ms.Format(g.Board)

Note that you must pass the game's current board to Format. This allows Format
to determine whether a move represents a capture and to disambiguate between
moves with the same notation.

To apply a move to a game, use the Game type's Apply method:

	m := ms[0]
	g = g.Apply(m)

Rather than mutating the game, Apply returns a new copy of the game's state
after applying the given move. This allows you to potentially build a graph of
game states linked by moves. Apply tries to be smart about updating move
counters and castling rights based on the move. (That is, if the move applies
to White's king, Apply will revoke White's castling rights.)
*/
package chess
