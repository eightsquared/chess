/*
Package fen provides functions for formatting a game in Forsyth–Edwards
Notation and for parsing that same notation into a game.

Forsyth–Edwards Notation, or FEN, describes a compact way to serialize the
current state of a standard game of chess so as to allow the game to be
properly resumed. Here, "properly resumed" means that we can accurately
determine the set of valid moves available to both players after setting up the
board according to the serialized description. This requires encoding certain
annotations that cannot be conveyed merely by the positions of pieces on the
board, such as which player has the move, which castling rights are available
to both players (since the positions of the kings and rooks alone is not
sufficient) and whether an en passant capture is possible on the next move
(since the positions of the pawns alone is not sufficient). FEN also provides
two move counters, one of which may be used to determine whether a draw may be
claimed due to a certain number of moves having elapsed since the last capture
or pawn move. (However, FEN does not provide sufficient information to
determine other draw conditions, such as repetition of a previously seen
position. This would require a full game history.)

FEN encodes information in a single line and uses a limited set of ASCII
characters (specifically: letters, digits, forward slashes, spaces, and
hyphens), making it easy to transcribe by hand and simple to transmit. A valid
FEN line consists of six tokens separated by a single space character:

	1. The board layout
	2. An indication of which player has the move
	3. Castling rights for both players
	4. The en passant "target square"
	5. The number of half moves since the last capture or pawn move
	6. The number of full moves since the start of the game

As an example, we will consider the Sveshnikov Variation of the Sicilian
Defense. After eight moves, the board looks like this:

	  ╔═══╤═══╤═══╤═══╤═══╤═══╤═══╤═══╗          White     Black
	8 ║ r │   │ b │ q │ k │ b │   │ r ║       1. e4        c5
	  ╟───┼───┼───┼───┼───┼───┼───┼───╢       2. Nf3       Nc6
	7 ║   │   │   │   │   │ p │ p │ p ║       3. d4        cxd4
	  ╟───┼───┼───┼───┼───┼───┼───┼───╢       4. Nxd4      Nf6
	6 ║ p │   │ n │ p │   │ n │   │   ║       5. Nc3       e5
	  ╟───┼───┼───┼───┼───┼───┼───┼───╢       6. Ndb5      d6
	5 ║   │ p │   │   │ p │   │ B │   ║       7. Bg5       a6
	  ╟───┼───┼───┼───┼───┼───┼───┼───╢       8. Na3       b5
	4 ║   │   │   │   │ P │   │   │   ║
	  ╟───┼───┼───┼───┼───┼───┼───┼───╢
	3 ║ N │   │ N │   │   │   │   │   ║
	  ╟───┼───┼───┼───┼───┼───┼───┼───╢
	2 ║ P │ P │ P │   │   │ P │ P │ P ║
	  ╟───┼───┼───┼───┼───┼───┼───┼───╢
	1 ║ R │   │   │ Q │ K │ B │   │ R ║
	  ╚═══╧═══╧═══╧═══╧═══╧═══╧═══╧═══╝
	    a   b   c   d   e   f   g   h

To encode the board, we encode each rank separately, starting from the top of
the board according to White's point of view. Each rank is read from left to
right. White pieces are represented by upper-case letters, while black pieces
are represented by lower-case letters, as used in the ASCII board above. Empty
squares are encoded using a single digit to represent the number of consecutive
empty squares. Thus the eighth rank of the example board would be formatted as
r1bqkb1r, while the seventh rank would become 5ppp. All encoded ranks are
separated by forward slashes, which yields the following representation of the
complete board:

	r1bqkb1r/5ppp/p1np1n2/1p2p1B1/4P3/N1N5/PPP2PPP/R2QKB1R

To this we append a single space character and either 'w' or 'b' depending on
which player has the move.

	r1bqkb1r/5ppp/p1np1n2/1p2p1B1/4P3/N1N5/PPP2PPP/R2QKB1R w

Next come the castling rights, which we represent by either some combination of
the characters K, Q, k, or q (for White kingside, White queenside, Black
kingside, and Black queenside, respectively), or a single hyphen if neither
side retains any castling rights. FEN does not specify the order of these
characters, so the castling rights expressed by KQkq could also be serialized
as qkQK. In the spirit of "be conservative in what you send and liberal in what
you accept", the Format function exported by this package will always indicate
White's castling rights before Black's and kingside castling rights before
queenside, while the Parse function is written in a lenient manner and will
accept any order. In the example above, neither player has moved the king or a
rook, so all castling rights are retained.

	r1bqkb1r/5ppp/p1np1n2/1p2p1B1/4P3/N1N5/PPP2PPP/R2QKB1R w KQkq

We now encode the en passant target square in standard notation. This value is
provided if and only if the preceding move was a two-square pawn advance. The
target square is defined as the square immediately behind the advanced pawn. In
this case, since Black's last move was 8…b5, the en passant target square is
b6. We provide this value even if the player with the move has no pawns in
position to capture en passant. Following any move that is not a two-square
pawn advance, we instead append a single hyphen in place of the en passant
target square.

	r1bqkb1r/5ppp/p1np1n2/1p2p1B1/4P3/N1N5/PPP2PPP/R2QKB1R w KQkq b6

For the move counters, we first provide the number of half-moves since the last
capture or pawn move (zero in this example) and the full move counter, which
starts at one and is incremented after Black's move (nine in this example).
Both counters are provided as base-10 numbers and separated by spaces. Thus the
complete FEN encoding of the above example game is:

	r1bqkb1r/5ppp/p1np1n2/1p2p1B1/4P3/N1N5/PPP2PPP/R2QKB1R w KQkq b6 0 9

The move counters are needed only to determine certain game-completion
conditions, such as allowing a draw only if a certain number of moves has
elapsed, or allowing a draw if more than a certain number of moves has elapsed
since the last capture or pawn move by either player. They are not needed to
determine the set of valid moves for a given position.

If the move counters are excluded, the FEN encoding of a game provides a
reasonably compact hash that uniquely identifies a single game state in the
universe of all possible positions. A single game (including variations) could
thus be described as a graph of states, where the nodes contain the FEN-encoded
positions and the edges are moves.
*/
package fen
