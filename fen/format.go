package fen

import (
	"gitlab.com/eightsquared/chess"
)

// Format returns the FEN encoding of the given game g.
func Format(g chess.Game) string {
	const (
		hyphen = '-'
		slash  = '/'
		space  = ' '
	)
	// FEN is a variable-length encoding, so we can't establish a precise upper
	// bound on the length of the final string. We can reasonably assume that
	// 120 bytes is enough unless the move counters are absolutely huge. To
	// avoid repeated allocations with append, we'll allocate the full slice
	// here. i will track the index of the next character in the string.
	b := make([]byte, 120)
	i := 0
	// For the board layout, we need a counter for the number of consecutive
	// empty squares we have encountered.
	empty := 0
	for s := chess.First; s <= chess.Last; s++ {
		if s > chess.First && s.File() == 'a' {
			// If the square we're looking at is in the leftmost file (and it's
			// not the first square in the loop), we need to write the number
			// of consecutive empty squares from the previous rank, if there
			// were any. Since this will never be more than eight, we can get
			// the correct digit by adding the rune representation of the
			// counter to the zero digit.
			if empty != 0 {
				b[i] = byte('0' + rune(empty))
				i++
				empty = 0
			}
			// Reaching the leftmost file also means that we need to write a
			// forward slash prior to starting the next row.
			b[i] = slash
			i++
		}
		// The current square is either empty (in which case we add one to the
		// empty square counter and continue), or it has a piece. In the latter
		// case we first write out any non-zero number of empty squares using
		// the same method as above before writing out the rune that represents
		// the piece.
		if g.Board[s] == chess.Empty {
			empty++
		} else {
			if empty != 0 {
				b[i] = byte('0' + rune(empty))
				i++
				empty = 0
			}
			b[i] = byte(g.Board[s].Rune())
			i++
		}
	}
	// If we reach the end of the board and we have a non-zero number of empty
	// squares, write that out before continuing.
	if empty > 0 {
		b[i] = byte('0' + rune(empty))
		i++
	}
	// Write out either 'w' or 'b' depending on which player has the move.
	b[i] = space
	i++
	if g.ToMove == chess.Black {
		b[i] = 'b'
		i++
	} else {
		b[i] = 'w'
		i++
	}
	// Write out the castling rights, or a hyphen if neither side has any
	// castling rights.
	b[i] = space
	i++
	if g.Castling.AllowsAny(chess.BothColorsBothSides) {
		if g.Castling.Allows(chess.WhiteKingside) {
			b[i] = 'K'
			i++
		}
		if g.Castling.Allows(chess.WhiteQueenside) {
			b[i] = 'Q'
			i++
		}
		if g.Castling.Allows(chess.BlackKingside) {
			b[i] = 'k'
			i++
		}
		if g.Castling.Allows(chess.BlackQueenside) {
			b[i] = 'q'
			i++
		}
	} else {
		b[i] = hyphen
		i++
	}
	// Write out the en passant target square, or a hyphen if the square is not
	// valid (such as None).
	b[i] = space
	i++
	if ep := g.EnPassant; ep.Valid() {
		b[i] = byte(ep.File())
		i++
		b[i] = byte(ep.Rank())
		i++
	} else {
		b[i] = hyphen
		i++
	}
	// Write out the move counters.
	b[i] = space
	i++
	var n, t uint16
	var z bool
	for n, t, z = g.HalfMovesSinceCaptureOrPawnMove, 1e4, false; t >= 10; t /= 10 {
		switch {
		case n >= t:
			q := n / t
			b[i] = '0' + byte(q)
			i++
			n -= q * t
			z = true
		case z:
			b[i] = '0'
			i++
		}
	}
	b[i] = '0' + byte(n)
	i++
	b[i] = space
	i++
	for n, t, z = g.FullMoves+1, 1e4, false; t >= 10; t /= 10 {
		switch {
		case n >= t:
			q := n / t
			b[i] = '0' + byte(q)
			i++
			n -= q * t
			z = true
		case z:
			b[i] = '0'
			i++
		}
	}
	b[i] = '0' + byte(n)
	i++
	return string(b[:i])
}
