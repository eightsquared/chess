package fen

import (
	"testing"

	"gitlab.com/eightsquared/chess"
)

func BenchmarkFormat(b *testing.B) {
	gs := make([]chess.Game, 1e5)
	for i := range gs {
		gs[i] = chess.RandomGame(i)
	}
	b.Run("bench", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			Format(gs[i%(len(gs))])
		}
	})
}
