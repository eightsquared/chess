package fen

import (
	"errors"
	"fmt"

	"gitlab.com/eightsquared/chess"
)

// Parse parses the given FEN-encoded string v into a game struct, returning an
// error if the given value is not in the expected format.
//
// This function is fairly lenient in what it will accept. It requires that v
// contains a valid board segment, where "valid" means that both all ranks are
// provided and each rank has the expected number of files. (No attempt is made
// to validate the resulting position to check that—for example—the board is
// not completely empty, or that one side does not have too many pieces or no
// king, however.) If the input ends after a valid board layout, this function
// will return no error, and will assume that White has the move, neither
// player retains castling rights, there is no en passant target square, and
// both move counters are zero. This may not be correct, but given the lack of
// other segments it is the best we can manage.
//
// Castling rights may be provided in any order: KQkq is considered the same as
// qkQK, which is considered the same as KkQq. However, duplicated rights, such
// as KK, are not allowed.
//
// Extra segments following the last expected segment are ignored, provided the
// extra segments are preceded by a recognized delimiter character. If the last
// expected segment is followed by a newline or other non-delimiter character,
// Parse will return an error. To avoid this, trim white space from both ends
// of v prior to calling Parse.
//
// In addition to the space character, Parse allows the underscore ('_') to be
// used as a delimiter between segments. This allows for embedding the value in
// a URL without requiring that the space characters be percent-encoded.
//
// Parse examines the given value on a byte-by-byte basis (not rune-by-rune).
// Passing a string with multibyte runes to Parse may produce unexpected
// results.
func Parse(v string) (game chess.Game, err error) {
	if v == "" {
		err = errors.New("parse: empty input")
		return
	}
	g := chess.Game{
		ToMove:    chess.White,
		EnPassant: chess.None,
	}
	s := &parseState{
		g:     &g,
		next:  pieceFlags | rankFlags,
		runes: boardRunes,
	}
	// Loop through all characters in v until we either run out of characters
	// or the parser index s.pi is incremented beyond the end of the parsers
	// slice.
	for ; s.ci < len(v) && s.pi < len(parsers); s.ci++ {
		s.c = v[s.ci]
		if int(s.c) >= len(s.runes) || s.runes[s.c]&s.next == 0 {
			err = fmt.Errorf("parse: unexpected character '%c'", s.c)
			return
		}
		s.flag = s.runes[s.c]
		parsers[s.pi](s)
	}
	// Exiting the above loop when the current square is still valid indicates
	// that the board parser did not finish parsing the board before we ran out
	// of characters.
	if s.s.Valid() {
		err = errors.New("parse: unexpected end of input")
		return
	}
	// FEN's full move counter starts at one prior to any moves having been made,
	// but it's more convenient for chess.Game.FullMoves to be zero-based in
	// order to count the number of completed full moves. This means we have to
	// subtract one from the parsed full move counter if we indeed parsed a value
	// (indicated by the counter being greater than zero).
	if g.FullMoves > 0 {
		g.FullMoves--
	}
	game = g
	return
}

// parseState describes the current state of parsing. Parsers are expected to
// take a pointer to this state and mutate it.
type parseState struct {
	// g refers to the game that is being constructed by parsing. Parsers are
	// expected to mutate this value each time a new piece of information is
	// parsed from the string.
	g *chess.Game
	// ci holds the current character index (byte index) in the string being
	// parsed. As this is incremented automatically, parsers generally have no
	// need to modify this index, but a parser may choose to decrement this index
	// prior to passing control to the next parser in order to "unread" a
	// character.
	ci int
	// pi holds the current parser index. Parsers may increment this value to
	// force the parsing controller to advance to the next parser in line.
	pi int
	// flag holds the flag value of the character c as determined by the map in
	// runes. Parsers should not modify this value.
	flag int
	// next is a bitmap that holds the flag (or combination of flags) that
	// describes the next expected character. Parsers are expected to set this
	// value. If the next character (converted to an integer flag by looking it
	// up in runes) is not part of this bitmap, it is considered an error.
	next int
	// runes maps characters to integer flags that uniquely identify characters.
	// Parsers are expected to assign the appropriote map of expected characters
	// to this value. If a character is encountered that is not part of this map,
	// it is considered an error.
	runes []int
	// c holds the character at the index ci in the string being parsed. Parsers
	// should not modify this value.
	c byte
	// s points to the current square under consideration. This is only relevant
	// to the parser that handles the board layout. However, it is expected that
	// after the board layout is parsed, s will refer to an invalid square. If s
	// still refers to a valid square when the end of input is encountered, it is
	// considered an error.
	s chess.Square
}

// A parser is a function that takes the current state of parsing. Parsers
// communicate with the controlling parse loop solely by mutating the given
// state.
type parser func(*parseState)

// These constants provide unique flags for each character in the set of
// characters found in a FEN-encoded string. Characters are mapped to these
// flags using the rune maps defined below.
const (
	spaceFlag int = 1 << iota
	slashFlag
	hyphenFlag
	underscoreFlag
	letterAFlag
	letterBFlag
	letterCFlag
	letterDFlag
	letterEFlag
	letterFFlag
	letterGFlag
	letterHFlag
	zeroFlag
	oneFlag
	twoFlag
	threeFlag
	fourFlag
	fiveFlag
	sixFlag
	sevenFlag
	eightFlag
	nineFlag
	whitePawnFlag
	whiteKnightFlag
	whiteBishopFlag
	whiteRookFlag
	whiteQueenFlag
	whiteKingFlag
	blackPawnFlag
	blackKnightFlag
	blackRookFlag
	blackQueenFlag
	blackKingFlag
	whiteFlag
	blackBishopFlag = letterBFlag
	blackFlag       = letterBFlag
)

// These composite flags help reduce typing.
const (
	whitePieceFlags = whitePawnFlag | whiteKnightFlag | whiteBishopFlag | whiteRookFlag | whiteQueenFlag | whiteKingFlag
	blackPieceFlags = blackPawnFlag | blackKnightFlag | blackBishopFlag | blackRookFlag | blackQueenFlag | blackKingFlag
	pieceFlags      = whitePieceFlags | blackPieceFlags
	castlingFlags   = whiteKingFlag | whiteQueenFlag | blackKingFlag | blackQueenFlag
	colorFlags      = whiteFlag | blackFlag
	fileFlags       = letterAFlag | letterBFlag | letterCFlag | letterDFlag | letterEFlag | letterFFlag | letterGFlag | letterHFlag
	rankFlags       = oneFlag | twoFlag | threeFlag | fourFlag | fiveFlag | sixFlag | sevenFlag | eightFlag
	digitFlags      = zeroFlag | oneFlag | twoFlag | threeFlag | fourFlag | fiveFlag | sixFlag | sevenFlag | eightFlag | nineFlag
	delimiterFlags  = spaceFlag | underscoreFlag
)

// boardRunes maps from the set of characters expected in the board layout
// segment to the appropriate flags.
var boardRunes = []int{
	'/': slashFlag,
	'P': whitePawnFlag,
	'N': whiteKnightFlag,
	'B': whiteBishopFlag,
	'R': whiteRookFlag,
	'Q': whiteQueenFlag,
	'K': whiteKingFlag,
	'p': blackPawnFlag,
	'n': blackKnightFlag,
	'b': blackBishopFlag,
	'r': blackRookFlag,
	'q': blackQueenFlag,
	'k': blackKingFlag,
	'1': oneFlag,
	'2': twoFlag,
	'3': threeFlag,
	'4': fourFlag,
	'5': fiveFlag,
	'6': sixFlag,
	'7': sevenFlag,
	'8': eightFlag,
}

// boardRemainingFlags maps from the number of squares remaining to be
// processed in the current rank to the appropriate flags. (For example, if the
// number of squares remaining is zero, meaning we have reached the end of the
// rank, we expect to see a slash character. If the number of squares remaining
// is three, we expect to see either a piece character, or a digit from one to
// three.)
var boardRemainingFlags = []int{
	0: slashFlag,
	1: pieceFlags | oneFlag,
	2: pieceFlags | oneFlag | twoFlag,
	3: pieceFlags | oneFlag | twoFlag | threeFlag,
	4: pieceFlags | oneFlag | twoFlag | threeFlag | fourFlag,
	5: pieceFlags | oneFlag | twoFlag | threeFlag | fourFlag | fiveFlag,
	6: pieceFlags | oneFlag | twoFlag | threeFlag | fourFlag | fiveFlag | sixFlag,
	7: pieceFlags | oneFlag | twoFlag | threeFlag | fourFlag | fiveFlag | sixFlag | sevenFlag,
	8: pieceFlags | oneFlag | twoFlag | threeFlag | fourFlag | fiveFlag | sixFlag | sevenFlag | eightFlag,
}

// pieceRunes maps from piece characters to the corresponding piece constant
// defined in the base package.
var pieceRunes = []chess.Piece{
	'P': chess.WhitePawn,
	'N': chess.WhiteKnight,
	'B': chess.WhiteBishop,
	'R': chess.WhiteRook,
	'Q': chess.WhiteQueen,
	'K': chess.WhiteKing,
	'p': chess.BlackPawn,
	'n': chess.BlackKnight,
	'b': chess.BlackBishop,
	'r': chess.BlackRook,
	'q': chess.BlackQueen,
	'k': chess.BlackKing,
}

// colorRunes maps from the set of characters expected in the color segment
// (including the delimiter) to the appropriate flags.
var colorRunes = []int{
	' ': spaceFlag,
	'_': underscoreFlag,
	'w': whiteFlag,
	'b': blackFlag,
}

// castlingRunes maps from the set of characters expected in the castling
// rights segment (including the delimiter) to the appropriate flags.
var castlingRunes = []int{
	' ': spaceFlag,
	'_': underscoreFlag,
	'-': hyphenFlag,
	'K': whiteKingFlag,
	'Q': whiteQueenFlag,
	'k': blackKingFlag,
	'q': blackQueenFlag,
}

// enPassantRunes maps from the set of characters expected in the en passant
// target square segment (including the delimiter) to the appropriate flags.
var enPassantRunes = []int{
	' ': spaceFlag,
	'_': underscoreFlag,
	'-': hyphenFlag,
	'a': letterAFlag,
	'b': letterBFlag,
	'c': letterCFlag,
	'd': letterDFlag,
	'e': letterEFlag,
	'f': letterFFlag,
	'g': letterGFlag,
	'h': letterHFlag,
	'3': threeFlag,
	'6': sixFlag,
}

// counterRunes maps from the set of characters expected in either of the move
// counter segments (including the delimiter) to the appropriate flags.
var counterRunes = []int{
	' ': spaceFlag,
	'_': underscoreFlag,
	'0': zeroFlag,
	'1': oneFlag,
	'2': twoFlag,
	'3': threeFlag,
	'4': fourFlag,
	'5': fiveFlag,
	'6': sixFlag,
	'7': sevenFlag,
	'8': eightFlag,
	'9': nineFlag,
}

// parsers holds the sequence of parse functions.
var parsers = []parser{
	parser(boardParser),
	parser(colorParser),
	parser(castlingParser),
	parser(enPassantParser),
	parser(halfMovesParser),
	parser(fullMovesParser),
}

// boardParser parses the board layout segment.
func boardParser(s *parseState) {
	if s.flag == slashFlag {
		// On encountering a slash character, we expect the next character to
		// be either a piece or any digit from one to eight.
		s.next = pieceFlags | rankFlags
		return
	}
	// Since the current square's file will be in the range 'a' to 'h', we can
	// subtract it from 'i' to get the number of squares remaining in the
	// current rank.
	rem := int('i' - s.s.File())
	if s.flag&pieceFlags != 0 {
		// A piece character causes us to advance one square forward and
		// reduces the number of remaining squares in the rank by one.
		s.g.Board[s.s] = pieceRunes[s.c]
		s.s++
		rem--
	} else {
		// Otherwise, a digit character causes us to advance some non-zero
		// number of squares forward and reduces the number of remaining
		// squares in the rank by that same number. Since s.c will be in the
		// range '1' to '8', we can capture the number of requested squares in
		// req by subtracting '0'. We know that this number will be valid (i.e.
		// it will not be greater than rem) because at the end of this function
		// we assign the set of expected characters based on the number of
		// squares remaining in the rank, so the parse loop will handle the
		// work of rejecting invalid characters.
		req := int(s.c - '0')
		s.s += chess.Square(req)
		rem -= req
	}
	if !s.s.Valid() {
		// If advancing the current square above results in an invalid square,
		// we know we have consumed the entire board and can advance to the
		// next parser.
		s.next = delimiterFlags
		s.runes = colorRunes
		s.pi++
		return
	}
	// Otherwise there is still at least one square remaining in the board, and
	// we choose the set of expected characters based on how many squares are
	// remaining in the current rank.
	s.next = boardRemainingFlags[rem]
}

// colorParser parses the color segment.
func colorParser(s *parseState) {
	if s.flag&delimiterFlags != 0 {
		// After receiving the delimiter that precedes this segment, we expect
		// to see one of the two color flags.
		s.next = colorFlags
		return
	}
	// Since the color segment is exactly one character in length, we simply
	// set the player with the move and advance to the next parser.
	if s.flag == whiteFlag {
		s.g.ToMove = chess.White
	} else if s.flag == blackFlag {
		s.g.ToMove = chess.Black
	}
	s.next = delimiterFlags
	s.runes = castlingRunes
	s.pi++
}

// castlingParser parses the castling rights segment.
func castlingParser(s *parseState) {
	if s.flag&delimiterFlags != 0 {
		// Receiving a delimiter character could indicate that we are either at
		// the delimiter prior to the segment or at the delimiter that follows
		// some non-empty set of castling rights. (The case where a hyphen is
		// provided to indicate no castling rights for either player is handled
		// below.) Testing the current value of the castling rights is
		// sufficient to determine which delimiter we are at.
		if s.g.Castling != 0 {
			// We are at the succeding delimiter and must unread one character
			// by decrementing s.ci before advancing to the next parser.
			s.ci--
			s.runes = enPassantRunes
			s.pi++
			return
		}
		// We are at the preceding delimiter and now expect to see either a
		// hyphen or one of the four castling flags.
		s.next = hyphenFlag | castlingFlags
		return
	}
	// A hyphen allows us to advance to the next parser immediately. Otherwise,
	// for each of the four castling flags, we grant that castling right and
	// remove that flag and the hyphen flag from the set of expected
	// characters.
	switch s.flag {
	case hyphenFlag:
		s.next = delimiterFlags
		s.runes = enPassantRunes
		s.pi++
		return
	case whiteKingFlag:
		s.g.Castling = s.g.Castling.Grant(chess.WhiteKingside)
		s.next &= ^(hyphenFlag | whiteKingFlag)
	case whiteQueenFlag:
		s.g.Castling = s.g.Castling.Grant(chess.WhiteQueenside)
		s.next &= ^(hyphenFlag | whiteQueenFlag)
	case blackKingFlag:
		s.g.Castling = s.g.Castling.Grant(chess.BlackKingside)
		s.next &= ^(hyphenFlag | blackKingFlag)
	case blackQueenFlag:
		s.g.Castling = s.g.Castling.Grant(chess.BlackQueenside)
		s.next &= ^(hyphenFlag | blackQueenFlag)
	}
	// After receiving at least one castling flag we also expect to potentially
	// encounter a delimiter signalling the end of the castling rights segment.
	s.next |= delimiterFlags
}

// enPassantParser parses the en passant target square segment.
func enPassantParser(s *parseState) {
	if s.flag&delimiterFlags != 0 {
		// After receiving the delimiter that precedes this segment, we expect
		// to see either a hyphen or a file character.
		s.next = hyphenFlag | fileFlags
		return
	}
	if s.flag == hyphenFlag {
		// A hyphen indicates that there is no en passant target square and we
		// can advance to the next parser.
		s.next = delimiterFlags
		s.runes = counterRunes
		s.pi++
		return
	}
	if s.flag&fileFlags != 0 {
		// Receiving a file flag allows us to at least choose the correct
		// column. We now expect to see either a six (if White has the move) or
		// a three (if Black has the move), either of which will allow us to
		// complete the assignment of the en passont target square.
		s.g.EnPassant = chess.A1.Add(chess.Offset{Files: int8(s.c - 'a'), Ranks: 0})
		if s.g.ToMove == chess.White {
			s.next = sixFlag
		} else {
			s.next = threeFlag
		}
		return
	}
	// Reaching this point means that s.c should be a rank character, so we
	// shift the current en passant target square up from the first rank to the
	// rank indicated by s.c and advance to the next parser.
	s.g.EnPassant = s.g.EnPassant.Add(chess.Offset{Files: 0, Ranks: int8(s.c - '1')})
	s.next = delimiterFlags
	s.runes = counterRunes
	s.pi++
}

// halfMovesParser parses the counter of half moves since the last capture or
// pawn move.
func halfMovesParser(s *parseState) {
	if s.flag&delimiterFlags != 0 {
		// Like the castling rights parser, a delimiter character could
		// represent either the preceding or the succeding delimiter. If we
		// have a non-zero value in the counter, we know we are at the
		// succeeding delimiter.
		if s.g.HalfMovesSinceCaptureOrPawnMove != 0 {
			s.pi++
			s.ci--
			return
		}
		s.next = digitFlags
		return
	}
	if s.flag == zeroFlag && s.g.HalfMovesSinceCaptureOrPawnMove == 0 {
		// If we encounter a zero and the half move counter is still zero, we
		// do not expect to see any other digits and can advance to the next
		// parser.
		s.next = delimiterFlags
		s.pi++
		return
	}
	// Otherwise we capture digits from left to right until encountering a
	// delimiter character.
	s.g.HalfMovesSinceCaptureOrPawnMove = (s.g.HalfMovesSinceCaptureOrPawnMove * 10) + uint16(s.c-'0')
	s.next = digitFlags | delimiterFlags
}

// fullMovesParser parses the counter of full moves since the start of the
// game.
func fullMovesParser(s *parseState) {
	// This parser follows the same pattern as halfMovesParser.
	if s.flag&delimiterFlags != 0 {
		if s.g.FullMoves != 0 {
			s.pi++
			s.ci--
			return
		}
		s.next = digitFlags & (^zeroFlag)
		return
	}
	s.g.FullMoves = (s.g.FullMoves * 10) + uint16(s.c-'0')
	s.next = digitFlags | delimiterFlags
}
