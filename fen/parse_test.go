package fen

import (
	"errors"
	"testing"

	"gitlab.com/eightsquared/chess"
)

func TestParse(t *testing.T) {
	t.Parallel()
	cases := []struct {
		title    string
		value    string
		err      error
		expected chess.Game
	}{
		{
			"empty input",
			"",
			errors.New("parse: empty input"),
			chess.Game{},
		},
		{
			"invalid character",
			"8/8/8/z/8/8/8/8",
			errors.New("parse: unexpected character 'z'"),
			chess.Game{},
		},
		{
			"too few rows",
			"8/8/8/8/8/8/8",
			errors.New("parse: unexpected end of input"),
			chess.Game{},
		},
		{
			"too many rows",
			"8/8/8/8/8/8/8/8/8",
			errors.New("parse: unexpected character '/'"),
			chess.Game{},
		},
		{
			"too many blank squares",
			"ppppppp2/8/8/8/8/8/8/8",
			errors.New("parse: unexpected character '2'"),
			chess.Game{},
		},
		{
			"too many pieces in row",
			"ppppppppp/8/8/8/8/8/8/8",
			errors.New("parse: unexpected character 'p'"),
			chess.Game{},
		},
		{
			"empty board",
			"8/8/8/8/8/8/8/8",
			nil,
			chess.Game{ToMove: chess.White, EnPassant: chess.None},
		},
		{
			"default initial board",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR",
			nil,
			chess.Game{
				Board:     chess.DefaultInitialBoard,
				ToMove:    chess.White,
				EnPassant: chess.None,
			},
		},
		{
			"invalid color character",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR a",
			errors.New("parse: unexpected character 'a'"),
			chess.Game{},
		},
		{
			"black to move",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b",
			nil,
			chess.Game{
				Board:     chess.DefaultInitialBoard,
				ToMove:    chess.Black,
				EnPassant: chess.None,
			},
		},
		{
			"no castling rights",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w -",
			nil,
			chess.Game{
				Board:     chess.DefaultInitialBoard,
				ToMove:    chess.White,
				EnPassant: chess.None,
			},
		},
		{
			"invalid castling rights",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w ?",
			errors.New("parse: unexpected character '?'"),
			chess.Game{},
		},
		{
			"white castling rights",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQ",
			nil,
			chess.Game{
				Board:     chess.DefaultInitialBoard,
				ToMove:    chess.White,
				Castling:  chess.WhiteBothSides,
				EnPassant: chess.None,
			},
		},
		{
			"black castling rights",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w kq",
			nil,
			chess.Game{
				Board:     chess.DefaultInitialBoard,
				ToMove:    chess.White,
				Castling:  chess.BlackBothSides,
				EnPassant: chess.None,
			},
		},
		{
			"no en passant target square",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - -",
			nil,
			chess.Game{
				Board:     chess.DefaultInitialBoard,
				ToMove:    chess.White,
				EnPassant: chess.None,
			},
		},
		{
			"invalid en passant target square",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - ?",
			errors.New("parse: unexpected character '?'"),
			chess.Game{},
		},
		{
			"invalid en passant rank (white to move)",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - a3",
			errors.New("parse: unexpected character '3'"),
			chess.Game{},
		},
		{
			"invalid en passant rank (black to move)",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b - a6",
			errors.New("parse: unexpected character '6'"),
			chess.Game{},
		},
		{
			"valid en passant rank (white to move)",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - a6",
			nil,
			chess.Game{
				Board:     chess.DefaultInitialBoard,
				ToMove:    chess.White,
				EnPassant: chess.A6,
			},
		},
		{
			"valid en passant rank (black to move)",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR b - a3",
			nil,
			chess.Game{
				Board:     chess.DefaultInitialBoard,
				ToMove:    chess.Black,
				EnPassant: chess.A3,
			},
		},
		{
			"invalid half move counter",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - - ?",
			errors.New("parse: unexpected character '?'"),
			chess.Game{},
		},
		{
			"valid half move counter",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - - 42",
			nil,
			chess.Game{
				Board:                           chess.DefaultInitialBoard,
				ToMove:                          chess.White,
				EnPassant:                       chess.None,
				HalfMovesSinceCaptureOrPawnMove: 42,
			},
		},
		{
			"invalid full move counter",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - - 0 ?",
			errors.New("parse: unexpected character '?'"),
			chess.Game{},
		},
		{
			"invalid full move counter (zero)",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - - 0 0",
			errors.New("parse: unexpected character '0'"),
			chess.Game{},
		},
		{
			"valid full move counter",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w - - 0 43",
			nil,
			chess.Game{
				Board:     chess.DefaultInitialBoard,
				ToMove:    chess.White,
				EnPassant: chess.None,
				FullMoves: 42,
			},
		},
		{
			"extra segments",
			"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 foobar",
			nil,
			chess.Game{
				Board:     chess.DefaultInitialBoard,
				ToMove:    chess.White,
				Castling:  chess.BothColorsBothSides,
				EnPassant: chess.None,
				FullMoves: 0,
			},
		},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			//fmt.Println(cases[i].title)
			expected := cases[i].expected
			actual, err := Parse(cases[i].value)
			if (err != nil && cases[i].err == nil) || (err == nil && cases[i].err != nil) {
				t.Errorf("error mismatch: expected %v; received %v", cases[i].err, err)
			} else if err != nil && cases[i].err != nil && err.Error() != cases[i].err.Error() {
				t.Errorf("error mismatch: expected %v; received %v", cases[i].err, err)
			}
			if !actual.Board.Equals(expected.Board) {
				t.Errorf("board mismatch: expected %s; received %s", expected.Board.String(), actual.Board.String())
			}
			if actual.ToMove != expected.ToMove {
				t.Errorf("to move mismatch: expected %c; received %c", expected.ToMove.Rune(), actual.ToMove.Rune())
			}
			if actual.Castling != expected.Castling {
				t.Errorf("castling mismatch: expected %v; received %v", expected.Castling, actual.Castling)
			}
			if actual.EnPassant != expected.EnPassant {
				t.Errorf("en passant mismatch: expected %s; received %s", expected.EnPassant, actual.EnPassant)
			}
			if actual.HalfMovesSinceCaptureOrPawnMove != expected.HalfMovesSinceCaptureOrPawnMove {
				t.Errorf("half moves mismatch: expected %d; received %d", expected.HalfMovesSinceCaptureOrPawnMove, actual.HalfMovesSinceCaptureOrPawnMove)
			}
			if actual.FullMoves != expected.FullMoves {
				t.Errorf("full moves mismatch: expected %d; received %d", expected.FullMoves, actual.FullMoves)
			}
		}
	}
	for i, c := range cases {
		t.Run(c.title, test(i))
	}
}

func TestFuzzing(t *testing.T) {
	if testing.Short() {
		return
	}
	t.Parallel()
	gs := make([]chess.Game, 1e6)
	for i := range gs {
		gs[i] = chess.RandomGame(i)
	}
	for _, g := range gs {
		v := Format(g)
		h, err := Parse(v)
		if err != nil {
			t.Fatalf("error parsing %s: %v", v, err)
		}
		if !g.Board.Equals(h.Board) {
			t.Fatalf("error parsing %s: boards not equal", v)
		}
		if g.ToMove != h.ToMove {
			t.Fatalf("error parsing %s: move color not equal", v)
		}
		if g.Castling != h.Castling {
			t.Fatalf("error parsing %s: castling rights not equal", v)
		}
		if g.EnPassant != h.EnPassant {
			t.Fatalf("error parsing %s: en passant target square not equal", v)
		}
		if g.HalfMovesSinceCaptureOrPawnMove != h.HalfMovesSinceCaptureOrPawnMove {
			t.Fatalf("error parsing %s: half move counters not equal", v)
		}
		if g.FullMoves != h.FullMoves {
			t.Fatalf("error parsing %s: full move counters not equal", v)
		}
	}
}

func BenchmarkParse(b *testing.B) {
	gs := make([]chess.Game, 1e5)
	for i := range gs {
		gs[i] = chess.RandomGame(i)
	}
	vs := make([]string, len(gs))
	for i := range gs {
		vs[i] = Format(gs[i])
	}
	b.Run("bench", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			_, err := Parse(vs[i%len(vs)])
			if err != nil {
				b.Log(err)
			}
		}
	})
}
