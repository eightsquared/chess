package chess

// Game represents a game of chess. The values in Game hold enough information
// to both determine valid moves and to serialize the game state in such a way
// that the game can be accurately resumed.
type Game struct {
	// Board describes the layout of the chess board.
	Board Board
	// ToMove is either White or Black and indicates which player has the move.
	ToMove Piece
	// Castling describes to which sides both white and black may castle.
	// Maintaining this value separately is required, as the position of pieces
	// on the board alone does not provide enough information to determine
	// whether one side or the other may castle.
	Castling CastlingRights
	// EnPassant holds the current en passant "target square"; that is, the
	// square immediately behind a pawn that advanced two squares in the
	// preceding move. If the preceding move was not a two-square pawn advance,
	// EnPassant will be None.
	EnPassant Square
	// HalfMovesSinceCaptureOrPawnMove holds (unsurprisingly) the number of half
	// moves that have elapsed since a capture or pawn move. This value is needed
	// to determine whether a game may be drawn due to this count reaching a
	// certain number (usually 100, or 50 full moves). Other draw conditions,
	// such as insufficient material to checkmate or repetition of the same
	// position, must be determined through other means.
	HalfMovesSinceCaptureOrPawnMove uint16
	// FullMoves holds the full move counter and is incremented after black
	// moves.
	FullMoves uint16
}

// Apply returns the state of the game g after the given move m has been
// applied to the board. This method updates the counters, castling rights, and
// other metadata, but it does not check that the given move is valid for the
// current state. If the validity of the move m is in doubt, it is the
// responsibility of the calling code to make this determination.
func (g Game) Apply(m Move) (h Game) {
	p := g.Board[m[0]]
	h.Board = g.Board.Apply(m)
	h.ToMove = g.ToMove.Invert()
	h.Castling = g.Castling
	h.EnPassant = None
	h.HalfMovesSinceCaptureOrPawnMove = g.HalfMovesSinceCaptureOrPawnMove + 1
	h.FullMoves = g.FullMoves
	if m.Captures(g.Board) {
		h.HalfMovesSinceCaptureOrPawnMove = 0
		// Capturing a rook revokes castling rights.
		c := g.Board[m[1]]
		if c.Is(Rook) {
			switch {
			case c.Is(White):
				if m[1] == H1 {
					h.Castling = h.Castling.Revoke(WhiteKingside)
				}
				if m[1] == A1 {
					h.Castling = h.Castling.Revoke(WhiteQueenside)
				}
			case c.Is(Black):
				if m[1] == H8 {
					h.Castling = h.Castling.Revoke(BlackKingside)
				}
				if m[1] == A8 {
					h.Castling = h.Castling.Revoke(BlackQueenside)
				}
			}
		}
	}
	if h.ToMove.Is(White) {
		h.FullMoves++
	}
	switch {
	case p.Is(Pawn):
		h.HalfMovesSinceCaptureOrPawnMove = 0
		if ep := m[0].Forward(p); ep.Forward(p) == m[1] {
			h.EnPassant = ep
		}
	case p.Is(Rook):
		// Moving the rook from the starting square revokes castling rights
		// from that side.
		switch {
		case p.Is(White):
			if m[0] == H1 {
				h.Castling = h.Castling.Revoke(WhiteKingside)
			}
			if m[0] == A1 {
				h.Castling = h.Castling.Revoke(WhiteQueenside)
			}
		case p.Is(Black):
			if m[0] == H8 {
				h.Castling = h.Castling.Revoke(BlackKingside)
			}
			if m[0] == A8 {
				h.Castling = h.Castling.Revoke(BlackQueenside)
			}
		}
	case p.Is(King):
		// Moving the king revokes castling rights from both sides.
		switch {
		case p.Is(White):
			h.Castling = h.Castling.Revoke(WhiteBothSides)
		case p.Is(Black):
			h.Castling = h.Castling.Revoke(BlackBothSides)
		}
	}
	return
}

// RandomGame generates a random board position and game state with the given
// seed. No consideration is given to whether the board is a valid state (pawns
// may end up on the first or last ranks, for example), but this function is
// useful for fuzz testing a large variety of scenarios.
func RandomGame(seed int) (g Game) {
	g.Board = DefaultInitialBoard.Shuffle()
	g.Castling = BothColorsBothSides & (0xf0 >> uint(seed%8))
	g.EnPassant = None
	g.HalfMovesSinceCaptureOrPawnMove = uint16(seed)
	g.FullMoves = uint16(seed % 256)
	if seed&1 == 0 {
		g.ToMove = White
		for s := A6; s <= H6; s++ {
			if g.Board[s] == Empty && g.Board[s.Up()] == Empty && g.Board[s.Down()] == BlackPawn {
				g.EnPassant = s
				break
			}
		}
	} else {
		g.ToMove = Black
		for s := A3; s <= H3; s++ {
			if g.Board[s] == Empty && g.Board[s.Down()] == Empty && g.Board[s.Up()] == WhitePawn {
				g.EnPassant = s
				break
			}
		}
	}
	return
}
