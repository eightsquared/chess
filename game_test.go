package chess

import (
	"fmt"
	"sort"
	"testing"
)

func TestGameApply(t *testing.T) {
	t.Parallel()
	t.Run("a pawn move resets the count of half moves since the last pawn move", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:  Board{E2: WhitePawn},
			ToMove: White,
			HalfMovesSinceCaptureOrPawnMove: 42,
		}
		move, expected := Move{E2, E4}, uint16(0)
		g = g.Apply(move)
		if actual := g.HalfMovesSinceCaptureOrPawnMove; actual != expected {
			t.Errorf("half moves since pawn move: expected %v; received %v", expected, actual)
		}
	})
	t.Run("a non-pawn move increments the count of half moves since the last pawn move", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:  Board{E1: WhiteQueen},
			ToMove: White,
			HalfMovesSinceCaptureOrPawnMove: 41,
		}
		move, expected := Move{E1, E2}, uint16(42)
		g = g.Apply(move)
		if actual := g.HalfMovesSinceCaptureOrPawnMove; actual != expected {
			t.Errorf("half moves since pawn move: expected %v; received %v", expected, actual)
		}
	})
	t.Run("a capturing move resets the count of half moves since the last capture", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:  Board{E2: WhitePawn, D3: BlackPawn},
			ToMove: White,
			HalfMovesSinceCaptureOrPawnMove: 42,
		}
		move, expected := Move{E2, D3}, uint16(0)
		g = g.Apply(move)
		if actual := g.HalfMovesSinceCaptureOrPawnMove; actual != expected {
			t.Errorf("half moves since pawn move: expected %v; received %v", expected, actual)
		}
	})
	t.Run("a non-capturing move increments the count of half moves since the last capture", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:  Board{D4: WhiteQueen},
			ToMove: White,
			HalfMovesSinceCaptureOrPawnMove: 41,
		}
		move, expected := Move{D4, A1}, uint16(42)
		g = g.Apply(move)
		if actual := g.HalfMovesSinceCaptureOrPawnMove; actual != expected {
			t.Errorf("half moves since pawn move: expected %v; received %v", expected, actual)
		}
	})
	t.Run("a two-space pawn move sets the en passant target square", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:  Board{E2: WhitePawn},
			ToMove: White,
		}
		move, expected := Move{E2, E4}, E3
		g = g.Apply(move)
		if actual := g.EnPassant; actual != expected {
			t.Errorf("en passant target square: expected %s; received %s", expected, actual)
		}
	})
	t.Run("moving the white rook from h1 revokes white's kingside castling right", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:    Board{E1: WhiteKing, A1: WhiteRook, H1: WhiteRook},
			ToMove:   White,
			Castling: BothColorsBothSides,
		}
		move, expected := Move{H1, H2}, WhiteQueenside|BlackBothSides
		g = g.Apply(move)
		if actual := g.Castling; actual != expected {
			t.Errorf("castling rights: expected %v; received %v", expected, actual)
		}
	})
	t.Run("capturing the white rook on h1 revokes white's kingside castling right", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:    Board{E1: WhiteKing, A1: WhiteRook, H1: WhiteRook, H2: BlackQueen},
			ToMove:   Black,
			Castling: BothColorsBothSides,
		}
		move, expected := Move{H2, H1}, WhiteQueenside|BlackBothSides
		g = g.Apply(move)
		if actual := g.Castling; actual != expected {
			t.Errorf("castling rights: expected %v; received %v", expected, actual)
		}
	})
	t.Run("moving the white rook from a1 revokes white's queenside castling right", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:    Board{E1: WhiteKing, A1: WhiteRook, H1: WhiteRook},
			ToMove:   White,
			Castling: BothColorsBothSides,
		}
		move, expected := Move{A1, A2}, WhiteKingside|BlackBothSides
		g = g.Apply(move)
		if actual := g.Castling; actual != expected {
			t.Errorf("castling rights: expected %v; received %v", expected, actual)
		}
	})
	t.Run("capturing the white rook on a1 revokes white's queenside castling right", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:    Board{E1: WhiteKing, A1: WhiteRook, H1: WhiteRook, A2: BlackQueen},
			ToMove:   Black,
			Castling: BothColorsBothSides,
		}
		move, expected := Move{A2, A1}, WhiteKingside|BlackBothSides
		g = g.Apply(move)
		if actual := g.Castling; actual != expected {
			t.Errorf("castling rights: expected %v; received %v", expected, actual)
		}
	})
	t.Run("moving the white king revokes white's castling rights", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:    Board{E1: WhiteKing, A1: WhiteRook, H1: WhiteRook},
			ToMove:   White,
			Castling: BothColorsBothSides,
		}
		move, expected := Move{E1, E2}, BlackBothSides
		g = g.Apply(move)
		if actual := g.Castling; actual != expected {
			t.Errorf("castling rights: expected %v; received %v", expected, actual)
		}
	})
	t.Run("moving the black rook from h8 revokes black's kingside castling right", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:    Board{E8: BlackKing, A8: BlackRook, H8: BlackRook},
			ToMove:   Black,
			Castling: BothColorsBothSides,
		}
		move, expected := Move{H8, H7}, BlackQueenside|WhiteBothSides
		g = g.Apply(move)
		if actual := g.Castling; actual != expected {
			t.Errorf("castling rights: expected %v; received %v", expected, actual)
		}
	})
	t.Run("capturing the black rook on h8 revokes black's kingside castling right", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:    Board{E8: BlackKing, A8: BlackRook, H8: BlackRook, H7: WhiteQueen},
			ToMove:   White,
			Castling: BothColorsBothSides,
		}
		move, expected := Move{H7, H8}, BlackQueenside|WhiteBothSides
		g = g.Apply(move)
		if actual := g.Castling; actual != expected {
			t.Errorf("castling rights: expected %v; received %v", expected, actual)
		}
	})
	t.Run("moving the black rook from a8 revokes black's queenside castling right", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:    Board{E8: BlackKing, A8: BlackRook, H8: BlackRook},
			ToMove:   Black,
			Castling: BothColorsBothSides,
		}
		move, expected := Move{A8, A7}, BlackKingside|WhiteBothSides
		g = g.Apply(move)
		if actual := g.Castling; actual != expected {
			t.Errorf("castling rights: expected %v; received %v", expected, actual)
		}
	})
	t.Run("capturing the black rook on a8 revokes black's queenside castling right", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:    Board{E8: BlackKing, A8: BlackRook, H8: BlackRook, A7: WhiteQueen},
			ToMove:   White,
			Castling: BothColorsBothSides,
		}
		move, expected := Move{A7, A8}, BlackKingside|WhiteBothSides
		g = g.Apply(move)
		if actual := g.Castling; actual != expected {
			t.Errorf("castling rights: expected %v; received %v", expected, actual)
		}
	})
	t.Run("moving the black king revokes black's castling rights", func(t *testing.T) {
		t.Parallel()
		g := Game{
			Board:    Board{E8: BlackKing, A8: BlackRook, H8: BlackRook},
			ToMove:   Black,
			Castling: BothColorsBothSides,
		}
		move, expected := Move{E8, E7}, WhiteBothSides
		g = g.Apply(move)
		if actual := g.Castling; actual != expected {
			t.Errorf("castling rights: expected %v; received %v", expected, actual)
		}
	})
}

func TestGameMoves(t *testing.T) {
	t.Parallel()
	moveFinders := []MoveFinder{new(StandardMoveFinder), new(BitwiseMoveFinder)}
	for _, strategy := range moveFinders {
		t.Run(fmt.Sprintf("strategy%v", strategy), func(t *testing.T) {
			t.Run("a white pawn", func(t *testing.T) {
				t.Parallel()
				t.Run("can advance one or two squares from second rank", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D2: WhitePawn},
						ToMove: White,
					}
					expected := Moves{
						Move{D2, D3},
						Move{D2, D4},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can advance only one square from second rank if fourth rank is occupied", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D2: WhitePawn, D4: BlackPawn},
						ToMove: White,
					}
					expected := Moves{
						Move{D2, D3},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can advance only one square from third rank", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D3: WhitePawn},
						ToMove: White,
					}
					expected := Moves{
						Move{D3, D4},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot advance if next rank is occupied", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D3: WhitePawn, D4: BlackPawn},
						ToMove: White,
					}
					expected := Moves{}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture to the left if square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D2: WhitePawn, D3: WhitePawn, D4: WhitePawn, D5: WhitePawn, D6: WhitePawn,
							C3: BlackPawn, C4: BlackPawn, C5: BlackPawn, C6: BlackPawn, C7: BlackPawn,
						},
						ToMove: White,
					}
					expected := Moves{
						Move{D2, C3},
						Move{D3, C4},
						Move{D4, C5},
						Move{D5, C6},
						Move{D6, C7},
						Move{D6, D7},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture to the left if square is en passant target square", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:     Board{D5: WhitePawn, C5: BlackPawn},
						ToMove:    White,
						EnPassant: C6,
					}
					expected := Moves{
						Move{D5, D6},
						Move{D5, C6, C5, None},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture to the right if square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D2: WhitePawn, D3: WhitePawn, D4: WhitePawn, D5: WhitePawn, D6: WhitePawn,
							E3: BlackPawn, E4: BlackPawn, E5: BlackPawn, E6: BlackPawn, E7: BlackPawn,
						},
						ToMove: White,
					}
					expected := Moves{
						Move{D2, E3},
						Move{D3, E4},
						Move{D4, E5},
						Move{D5, E6},
						Move{D6, E7},
						Move{D6, D7},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture to the right if square is en passant target square", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:     Board{D5: WhitePawn, E5: BlackPawn},
						ToMove:    White,
						EnPassant: E6,
					}
					expected := Moves{
						Move{D5, D6},
						Move{D5, E6, E5, None},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can promote by advancing from seventh rank if next rank is empty", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D7: WhitePawn},
						ToMove: White,
					}
					expected := Moves{
						Move{D7, D8, ReplaceWithKnight, D8},
						Move{D7, D8, ReplaceWithBishop, D8},
						Move{D7, D8, ReplaceWithRook, D8},
						Move{D7, D8, ReplaceWithQueen, D8},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can promote by capturing to the left from seventh rank if square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D7: WhitePawn, C8: BlackKnight, D8: BlackKnight},
						ToMove: White,
					}
					expected := Moves{
						Move{D7, C8, ReplaceWithKnight, C8},
						Move{D7, C8, ReplaceWithBishop, C8},
						Move{D7, C8, ReplaceWithRook, C8},
						Move{D7, C8, ReplaceWithQueen, C8},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can promote by capturing to the right from seventh rank if square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D7: WhitePawn, E8: BlackKnight, D8: BlackKnight},
						ToMove: White,
					}
					expected := Moves{
						Move{D7, E8, ReplaceWithKnight, E8},
						Move{D7, E8, ReplaceWithBishop, E8},
						Move{D7, E8, ReplaceWithRook, E8},
						Move{D7, E8, ReplaceWithQueen, E8},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
			})
			t.Run("a black pawn", func(t *testing.T) {
				t.Parallel()
				t.Run("can advance one or two squares from seventh rank", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D7: BlackPawn},
						ToMove: Black,
					}
					expected := Moves{
						Move{D7, D6},
						Move{D7, D5},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can advance only one square from second rank if fourth rank is occupied", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D7: BlackPawn, D5: WhitePawn},
						ToMove: Black,
					}
					expected := Moves{
						Move{D7, D6},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can advance only one square from sixth rank", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D6: BlackPawn},
						ToMove: Black,
					}
					expected := Moves{
						Move{D6, D5},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot advance if next rank is occupied", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D6: BlackPawn, D5: WhitePawn},
						ToMove: Black,
					}
					expected := Moves{}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture to the left if square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D7: BlackPawn, D6: BlackPawn, D5: BlackPawn, D4: BlackPawn, D3: BlackPawn,
							C6: WhitePawn, C5: WhitePawn, C4: WhitePawn, C3: WhitePawn, C2: WhitePawn,
						},
						ToMove: Black,
					}
					expected := Moves{
						Move{D7, C6},
						Move{D6, C5},
						Move{D5, C4},
						Move{D4, C3},
						Move{D3, C2},
						Move{D3, D2},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture to the left if square is en passant target square", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:     Board{D4: BlackPawn, C4: WhitePawn},
						ToMove:    Black,
						EnPassant: C3,
					}
					expected := Moves{
						Move{D4, D3},
						Move{D4, C3, C4, None},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture to the right if square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D7: BlackPawn, D6: BlackPawn, D5: BlackPawn, D4: BlackPawn, D3: BlackPawn,
							E6: WhitePawn, E5: WhitePawn, E4: WhitePawn, E3: WhitePawn, E2: WhitePawn,
						},
						ToMove: Black,
					}
					expected := Moves{
						Move{D7, E6},
						Move{D6, E5},
						Move{D5, E4},
						Move{D4, E3},
						Move{D3, E2},
						Move{D3, D2},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture to the right if square is en passant target square", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:     Board{D4: BlackPawn, E4: WhitePawn},
						ToMove:    Black,
						EnPassant: E3,
					}
					expected := Moves{
						Move{D4, D3},
						Move{D4, E3, E4, None},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can promote by advancing from second rank if next rank is empty", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D2: BlackPawn},
						ToMove: Black,
					}
					expected := Moves{
						Move{D2, D1, ReplaceWithKnight, D1},
						Move{D2, D1, ReplaceWithBishop, D1},
						Move{D2, D1, ReplaceWithRook, D1},
						Move{D2, D1, ReplaceWithQueen, D1},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can promote by capturing to the left from second rank if square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D2: BlackPawn, C1: WhiteKnight, D1: WhiteKnight},
						ToMove: Black,
					}
					expected := Moves{
						Move{D2, C1, ReplaceWithKnight, C1},
						Move{D2, C1, ReplaceWithBishop, C1},
						Move{D2, C1, ReplaceWithRook, C1},
						Move{D2, C1, ReplaceWithQueen, C1},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can promote by capturing to the right from second rank if square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D2: BlackPawn, E1: WhiteKnight, D1: WhiteKnight},
						ToMove: Black,
					}
					expected := Moves{
						Move{D2, E1, ReplaceWithKnight, E1},
						Move{D2, E1, ReplaceWithBishop, E1},
						Move{D2, E1, ReplaceWithRook, E1},
						Move{D2, E1, ReplaceWithQueen, E1},
					}
					if actual := strategy.Moves(g); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
			})
			t.Run("a knight", func(t *testing.T) {
				t.Parallel()
				t.Run("can move even if surrounded", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D4: WhiteKnight,
							D5: BlackPawn, E5: BlackPawn, E4: BlackPawn, E3: BlackPawn,
							D3: BlackPawn, C3: BlackPawn, C4: BlackPawn, C5: BlackPawn,
						},
						ToMove: White,
					}
					expected := Moves{
						Move{D4, E6},
						Move{D4, F5},
						Move{D4, F3},
						Move{D4, E2},
						Move{D4, C2},
						Move{D4, B3},
						Move{D4, B5},
						Move{D4, C6},
					}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture if arrival square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D4: WhiteKnight,
							E6: BlackPawn, F5: BlackPawn, F3: BlackPawn, E2: BlackPawn,
							C2: BlackPawn, B3: BlackPawn, B5: BlackPawn, C6: BlackPawn,
						},
						ToMove: White,
					}
					expected := Moves{
						Move{D4, E6},
						Move{D4, F5},
						Move{D4, F3},
						Move{D4, E2},
						Move{D4, C2},
						Move{D4, B3},
						Move{D4, B5},
						Move{D4, C6},
					}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move if arrival square is occupied by same color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D4: WhiteKnight,
							E6: WhitePawn, F5: WhitePawn, F3: WhitePawn, E2: WhitePawn,
							C2: WhitePawn, B3: WhitePawn, B5: WhitePawn, C6: WhitePawn,
						},
						ToMove: White,
					}
					expected := Moves{}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
			})
			t.Run("a bishop", func(t *testing.T) {
				t.Parallel()
				t.Run("can move on diagonals", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D4: WhiteBishop},
						ToMove: White,
					}
					expected := Moves{
						Move{D4, E5}, Move{D4, F6}, Move{D4, G7}, Move{D4, H8},
						Move{D4, E3}, Move{D4, F2}, Move{D4, G1},
						Move{D4, C3}, Move{D4, B2}, Move{D4, A1},
						Move{D4, C5}, Move{D4, B6}, Move{D4, A7},
					}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture if arrival square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D4: WhiteBishop,
							E5: BlackPawn, E3: BlackPawn, C3: BlackPawn, C5: BlackPawn,
						},
						ToMove: White,
					}
					expected := Moves{
						Move{D4, E5},
						Move{D4, E3},
						Move{D4, C3},
						Move{D4, C5},
					}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move to arrival square if occupied by same color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D4: WhiteBishop,
							E5: WhitePawn, E3: WhitePawn, C3: WhitePawn, C5: WhitePawn,
						},
						ToMove: White,
					}
					expected := Moves{}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
			})
			t.Run("a rook", func(t *testing.T) {
				t.Parallel()
				t.Run("can move on same file or rank", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D4: WhiteRook},
						ToMove: White,
					}
					expected := Moves{
						Move{D4, D5}, Move{D4, D6}, Move{D4, D7}, Move{D4, D8},
						Move{D4, E4}, Move{D4, F4}, Move{D4, G4}, Move{D4, H4},
						Move{D4, D3}, Move{D4, D2}, Move{D4, D1},
						Move{D4, C4}, Move{D4, B4}, Move{D4, A4},
					}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture if arrival square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D4: WhiteRook,
							D5: BlackPawn, E4: BlackPawn, D3: BlackPawn, C4: BlackPawn,
						},
						ToMove: White,
					}
					expected := Moves{
						Move{D4, D5},
						Move{D4, E4},
						Move{D4, D3},
						Move{D4, C4},
					}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move to arrival square if occupied by same color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D4: WhiteRook,
							D5: WhitePawn, E4: WhitePawn, D3: WhitePawn, C4: WhitePawn,
						},
						ToMove: White,
					}
					expected := Moves{}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
			})
			t.Run("a queen", func(t *testing.T) {
				t.Parallel()
				t.Run("can move on diagonals, same file, or same rank", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D4: WhiteQueen},
						ToMove: White,
					}
					expected := Moves{
						Move{D4, E5}, Move{D4, F6}, Move{D4, G7}, Move{D4, H8},
						Move{D4, E3}, Move{D4, F2}, Move{D4, G1},
						Move{D4, C3}, Move{D4, B2}, Move{D4, A1},
						Move{D4, C5}, Move{D4, B6}, Move{D4, A7},
						Move{D4, D5}, Move{D4, D6}, Move{D4, D7}, Move{D4, D8},
						Move{D4, E4}, Move{D4, F4}, Move{D4, G4}, Move{D4, H4},
						Move{D4, D3}, Move{D4, D2}, Move{D4, D1},
						Move{D4, C4}, Move{D4, B4}, Move{D4, A4},
					}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture if arrival square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D4: WhiteQueen,
							D5: BlackPawn, E5: BlackPawn, E4: BlackPawn, E3: BlackPawn,
							D3: BlackPawn, C3: BlackPawn, C4: BlackPawn, C5: BlackPawn,
						},
						ToMove: White,
					}
					expected := Moves{
						Move{D4, E5},
						Move{D4, E3},
						Move{D4, C3},
						Move{D4, C5},
						Move{D4, D5},
						Move{D4, E4},
						Move{D4, D3},
						Move{D4, C4},
					}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move if surrounded by same color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D4: WhiteQueen,
							D5: WhitePawn, E5: WhitePawn, E4: WhitePawn, E3: WhitePawn,
							D3: WhitePawn, C3: WhitePawn, C4: WhitePawn, C5: WhitePawn,
						},
						ToMove: White,
					}
					expected := Moves{}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
			})
			t.Run("a king", func(t *testing.T) {
				t.Parallel()
				t.Run("can move one square on diagonals, same file, or same rank", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D4: WhiteKing},
						ToMove: White,
					}
					expected := Moves{
						Move{D4, D5},
						Move{D4, E5},
						Move{D4, E4},
						Move{D4, E3},
						Move{D4, D3},
						Move{D4, C3},
						Move{D4, C4},
						Move{D4, C5},
					}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can capture if arrival square is occupied by opposite color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{D4: WhiteKing, E5: BlackPawn},
						ToMove: White,
					}
					expected := Moves{
						Move{D4, D5},
						Move{D4, E5},
						Move{D4, E4},
						Move{D4, E3},
						Move{D4, D3},
						Move{D4, C3},
						Move{D4, C4},
						Move{D4, C5},
					}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move if surrounded by same color", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board: Board{
							D4: WhiteKing,
							D5: WhitePawn, E5: WhitePawn, E4: WhitePawn, E3: WhitePawn,
							D3: WhitePawn, C3: WhitePawn, C4: WhitePawn, C5: WhitePawn,
						},
						ToMove: White,
					}
					expected := Moves{}
					if actual := strategy.Moves(g).From(D4); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move to a square attacked by a pawn (white king)", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{A1: WhiteKing, B3: BlackPawn},
						ToMove: White,
					}
					expected := Moves{Move{A1, B2}, Move{A1, B1}}
					if actual := strategy.Moves(g).From(A1); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move to a square attacked by a pawn (black king)", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{A8: BlackKing, B6: WhitePawn},
						ToMove: Black,
					}
					expected := Moves{Move{A8, B8}, Move{A8, B7}}
					if actual := strategy.Moves(g).From(A8); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move to a square attacked by a knight", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{A1: WhiteKing, A3: BlackKnight},
						ToMove: White,
					}
					expected := Moves{Move{A1, A2}, Move{A1, B2}}
					if actual := strategy.Moves(g).From(A1); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move to a square attacked by a bishop", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{A1: WhiteKing, B3: BlackBishop},
						ToMove: White,
					}
					expected := Moves{Move{A1, B2}, Move{A1, B1}}
					if actual := strategy.Moves(g).From(A1); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move to a square attacked by a rook", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{A1: WhiteKing, B3: BlackRook},
						ToMove: White,
					}
					expected := Moves{Move{A1, A2}}
					if actual := strategy.Moves(g).From(A1); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move to a square attacked by a queen", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{A1: WhiteKing, B3: BlackQueen},
						ToMove: White,
					}
					expected := Moves{}
					if actual := strategy.Moves(g).From(A1); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot move to a square attacked by a king", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{A1: WhiteKing, B3: BlackKing},
						ToMove: White,
					}
					expected := Moves{Move{A1, B1}}
					if actual := strategy.Moves(g).From(A1); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("can move to a square protected by another piece", func(t *testing.T) {
					t.Parallel()
					g := Game{
						Board:  Board{A1: WhiteKing, D3: BlackQueen, C2: BlackRook},
						ToMove: White,
					}
					expected := Moves{Move{A1, B1}}
					if actual := strategy.Moves(g).From(A1); !expected.Equals(actual) {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
			})
			t.Run("a white king", func(t *testing.T) {
				t.Parallel()
				t.Run("cannot castle kingside", func(t *testing.T) {
					t.Parallel()
					castle := Move{E1, G1, H1, F1}
					t.Run("if it is not on e1", func(t *testing.T) {
						g := Game{
							Board:    Board{D4: WhiteKing, H1: WhiteRook},
							Castling: WhiteKingside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(D4).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if it has no kingside castling right", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, H1: WhiteRook},
							Castling: WhiteQueenside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if f1 is occupied", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, H1: WhiteRook, F1: WhiteBishop},
							Castling: WhiteKingside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if g1 is occupied", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, H1: WhiteRook, G1: WhiteKnight},
							Castling: WhiteKingside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if it is attacked", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, H1: WhiteRook, E2: BlackRook},
							Castling: WhiteKingside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if f1 is attacked", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, H1: WhiteRook, E2: BlackQueen},
							Castling: WhiteKingside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
				})
				t.Run("can castle kingside if all conditions are met", func(t *testing.T) {
					t.Parallel()
					castle := Move{E1, G1, H1, F1}
					g := Game{
						Board:    Board{E1: WhiteKing, H1: WhiteRook},
						Castling: WhiteKingside,
						ToMove:   White,
					}
					expected := true
					if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot castle queenside", func(t *testing.T) {
					t.Parallel()
					castle := Move{E1, C1, A1, D1}
					t.Run("if it is not on e1", func(t *testing.T) {
						g := Game{
							Board:    Board{D4: WhiteKing, A1: WhiteRook},
							Castling: WhiteQueenside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(D4).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if it has no queenside castling right", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, A1: WhiteRook},
							Castling: WhiteKingside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if d1 is occupied", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, A1: WhiteRook, D1: WhiteQueen},
							Castling: WhiteQueenside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if c1 is occupied", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, A1: WhiteRook, C1: WhiteBishop},
							Castling: WhiteQueenside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if b1 is occupied", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, A1: WhiteRook, B1: WhiteKnight},
							Castling: WhiteQueenside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if it is attacked", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, A1: WhiteRook, E2: BlackRook},
							Castling: WhiteQueenside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if d1 is attacked", func(t *testing.T) {
						g := Game{
							Board:    Board{E1: WhiteKing, A1: WhiteRook, E2: BlackQueen},
							Castling: WhiteQueenside,
							ToMove:   White,
						}
						expected := false
						if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
				})
				t.Run("can castle queenside if all conditions are met", func(t *testing.T) {
					t.Parallel()
					castle := Move{E1, C1, A1, D1}
					g := Game{
						Board:    Board{E1: WhiteKing, A1: WhiteRook},
						Castling: WhiteQueenside,
						ToMove:   White,
					}
					expected := true
					if actual := strategy.Moves(g).From(E1).Has(castle); actual != expected {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
			})
			t.Run("a black king", func(t *testing.T) {
				t.Parallel()
				t.Run("cannot castle kingside", func(t *testing.T) {
					t.Parallel()
					castle := Move{E8, G8, H8, F8}
					t.Run("if it is not on e8", func(t *testing.T) {
						g := Game{
							Board:    Board{D4: BlackKing, H8: BlackRook},
							Castling: BlackKingside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(D4).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if it has no kingside castling right", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, H8: BlackRook},
							Castling: BlackQueenside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if f8 is occupied", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, H8: BlackRook, F8: BlackBishop},
							Castling: BlackKingside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if g8 is occupied", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, H8: BlackRook, G8: BlackKnight},
							Castling: BlackKingside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if it is attacked", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, H8: BlackRook, E7: WhiteRook},
							Castling: BlackKingside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if f8 is attacked", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, H8: BlackRook, E7: WhiteQueen},
							Castling: BlackKingside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
				})
				t.Run("can castle kingside if all conditions are met", func(t *testing.T) {
					t.Parallel()
					castle := Move{E8, G8, H8, F8}
					g := Game{
						Board:    Board{E8: BlackKing, H8: BlackRook},
						Castling: BlackKingside,
						ToMove:   Black,
					}
					expected := true
					if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
				t.Run("cannot castle queenside", func(t *testing.T) {
					t.Parallel()
					castle := Move{E8, C8, A8, D8}
					t.Run("if it is not on e8", func(t *testing.T) {
						g := Game{
							Board:    Board{D4: BlackKing, A8: BlackRook},
							Castling: BlackQueenside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(D4).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if it has no queenside castling right", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, A8: BlackRook},
							Castling: BlackKingside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if d8 is occupied", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, A8: BlackRook, D8: BlackQueen},
							Castling: BlackQueenside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if c8 is occupied", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, A8: BlackRook, C8: BlackBishop},
							Castling: BlackQueenside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if b8 is occupied", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, A8: BlackRook, B8: BlackKnight},
							Castling: BlackQueenside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if it is attacked", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, A8: BlackRook, E7: WhiteRook},
							Castling: BlackQueenside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
					t.Run("if d8 is attacked", func(t *testing.T) {
						g := Game{
							Board:    Board{E8: BlackKing, A8: BlackRook, E7: WhiteQueen},
							Castling: BlackQueenside,
							ToMove:   Black,
						}
						expected := false
						if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
							t.Errorf("expected %v; received %v", expected, actual)
						}
					})
				})
				t.Run("can castle queenside if all conditions are met", func(t *testing.T) {
					t.Parallel()
					castle := Move{E8, C8, A8, D8}
					g := Game{
						Board:    Board{E8: BlackKing, A8: BlackRook},
						Castling: BlackQueenside,
						ToMove:   Black,
					}
					expected := true
					if actual := strategy.Moves(g).From(E8).Has(castle); actual != expected {
						t.Errorf("expected %v; received %v", expected, actual)
					}
				})
			})
			t.Run("a piece cannot move if moving puts its own king in check", func(t *testing.T) {
				t.Parallel()
				g := Game{
					Board:  Board{E1: WhiteKing, F1: WhiteBishop, G1: BlackQueen},
					ToMove: White,
				}
				expected := Moves{}
				if actual := strategy.Moves(g).From(F1); !actual.Equals(expected) {
					t.Errorf("expected %v; received %v", expected, actual)
				}
			})
		})
	}
}

func BenchmarkGameMoves(b *testing.B) {
	gs := make([]Game, 10000)
	for i := range gs {
		gs[i] = RandomGame(i)
	}
	b.Run("standard", func(b *testing.B) {
		strategy := new(StandardMoveFinder)
		for i := 0; i < b.N; i++ {
			strategy.Moves(gs[i%len(gs)])
		}
	})
	b.Run("bitwise", func(b *testing.B) {
		strategy := new(BitwiseMoveFinder)
		for i := 0; i < b.N; i++ {
			strategy.Moves(gs[i%len(gs)])
		}
	})
}

func TestMoveFinders_Fuzzing(t *testing.T) {
	if testing.Short() {
		return
	}
	t.Parallel()
	standard := new(StandardMoveFinder)
	bitwise := new(BitwiseMoveFinder)
	is := make([]struct{}, 100000)
	for i := range is {
		g := RandomGame(i)
		standardMoves := standard.Moves(g)
		bitwiseMoves := bitwise.Moves(g)
		if !standardMoves.Equals(bitwiseMoves) {
			sort.Sort(standardMoves)
			sort.Sort(bitwiseMoves)
			t.Fatalf("moves not equal!\n%s\nstandard: %v\nbitwise: %v\n",
				g.Board.String(),
				standardMoves, bitwiseMoves)
		}
	}
}
