package chess

import (
	"unicode"
)

// Move represents the movement of a single piece across the board. Although
// represented as a slice, a move must have at least two squares to be valid.
// The simplest possible move has exactly two squares: the departure square and
// the arrival square, in that order. After applying this move to a given
// board, the content of the arrival square will be replaced with the content
// of the departure square, and the departure square will be rendered empty.
//
// More complex moves can be expressed through additional pairs of squares. For
// example, a pawn capturing en passant arrives at an empty square; to indicate
// that the opposing pawn must be removed, therefore, the move can be extended
// with the opposing pawn's square and the special None square, in that order.
// This pairing of Square + None provides a general method to indicate the
// removal of pieces from the board when this removal is not already implicitly
// expressed by the piece arriving on an occupied square.
//
// Similarly, castling can be expressed by the first pair of squares denoting
// the king's move, while a second pair of squares provides the rook's move.
type Move []Square

// Captures determines whether the move m, if applied to the given board b,
// will result in the removal of any piece from the board. This method does not
// take into account whether the capture is a valid move according to the
// current rules.
func (m Move) Captures(b Board) bool {
	for i := 1; i < len(m); i += 2 {
		s, t := m[i-1], m[i]
		if s.Valid() && t.Valid() && b[t] != Empty {
			return true
		}
		if s.Valid() && !t.Valid() {
			return true
		}
	}
	return false
}

// Equals determines whether the move m is the same as the given move n. Two
// moves are equal if they are of the same length and match square-for-square.
func (m Move) Equals(n Move) bool {
	if len(m) != len(n) {
		return false
	}
	for i := 0; i < len(m); i++ {
		if m[i] != n[i] {
			return false
		}
	}
	return true
}

// formatShortest returns the shortest possible serialized version of the move
// m when formatted according to Standard Algebraic Notation given the board b.
// The return value `more` indicates whether there could be more serialized
// versions of the move (such as to disambiguate between moves of the same
// piece class to the same square from the same rank or file). `more` will
// always be false for pawn and castling moves.
func (m Move) formatShortest(b Board) (f []byte, more bool) {
	// Ensure that the move has at least two squares and both the first squares
	// refer to valid locations on the board before attempting anything.
	if len(m) < 2 || !m[0].Valid() || !m[1].Valid() {
		return
	}
	// d and a hold the departure and arrival squares, respectively, while p
	// holds the piece currently at the departure square.
	d, a := m[0], m[1]
	p := b[d]
	if p == Empty {
		return
	}
	// af and ar hold the file and rank characters for the arrival square.
	af, ar := byte(a.File()), byte(a.Rank())
	switch {
	case p.Is(Pawn):
		// For pawn moves, we have a variety of cases to consider. First, if the
		// move has at least four squares, the second pair of squares could
		// indicate a promotion. In this case, the piece to which the pawn is
		// promoted is expected to be represented by the third square, and the
		// fourth square is expected to match the arrival square.
		rep := p
		if len(m) >= 4 && m[3] == a {
			rep = m[2].Replacement(p)
		}
		switch {
		case b[a] != Empty:
			// If the arrival square is not empty, we have a regular capture in the
			// format {departure file}x{arrival file}{arrival rank}. If we detected a
			// promotion piece above, it is appended to the end.
			df := byte(d.File())
			if len(m) >= 4 && p != rep {
				f = []byte{df, 'x', af, ar, byte(unicode.ToUpper(rep.Rune()))}
			} else {
				f = []byte{df, 'x', af, ar}
			}
		case len(m) >= 4 && m[2].Valid() && b[m[2]] == p.Invert() && m[3] == None:
			// If the arrival square is empty, but there is a second pair of squares
			// in the move that provides the location of an opposing pawn and the
			// special None square, we can assume that the move describes an en
			// passant capture. (We also assume that no promotions are possible with
			// en passant captures, and that the move has already been checked for
			// validity.)
			df := byte(d.File())
			f = []byte{df, 'x', af, ar, ' ', 'e', '.', 'p', '.'}
		default:
			// Otherwise we have a pawn advance in the form {arrival file}{arrival
			// rank}. This too may be accompanied by a promotion.
			if len(m) >= 4 && p != rep {
				f = []byte{af, ar, byte(unicode.ToUpper(rep.Rune()))}
			} else {
				f = []byte{af, ar}
			}
		}
	case p.Is(King) && len(m) >= 4 && m[2].Valid() && m[3].Valid() && b[m[2]].Is(p.Color()|Rook):
		// The move appears to describe a castling maneuver, as the first piece is
		// a king and the second piece is a rook of the same color. We will not
		// attempt to check the validity of the move given the current board state
		// (we assume this has already been done). Instead, we check to see whether
		// the king's arrival square is to the right or to the left of the
		// departure square, which indicates a kingside or queenside castle,
		// respectively.
		if a.File() > d.File() {
			f = []byte("0-0")
		} else {
			f = []byte("0-0-0")
		}
	default:
		// Failing the above cases, we have a regular piece move in the format
		// {piece}(x){arrival file}{arrival rank}, with 'x' being inserted if the
		// arrival square is not empty. We set more to true, as an alternate
		// formatting may be required to differentiate between other valid moves.
		more = true
		pr := byte(unicode.ToUpper(p.Rune()))
		if b[a] != Empty {
			f = []byte{pr, 'x', af, ar}
		} else {
			f = []byte{pr, af, ar}
		}
	}
	return
}

// formatAll returns the move m formatted according to Standard Algebraic
// Notation given the board b. The return value is a slice of byte slices,
// where the outer slice will be either of length 1 or length 4, depending on
// the type of move. In the event more than one byte slice is returned, the
// first such slice will hold the shortest possible move notation, while each
// successive slice will hold "tiebreaker" notations in case multiple moves on
// the same board have the same short form. Tiebreakers are ordered according
// to SAN rules. For example, for the move knight on b1 to c3, the returned
// byte slices will be "Nc3", "Nbc3", "N1c3", and "Nb1c3". It is up to the
// calling code to determine which slice to use given the current board state.
func (m Move) formatAll(b Board) (p [][]byte) {
	p0, more := m.formatShortest(b)
	if p0 == nil {
		return
	}
	// In the simplest case, formatShortest will return more == false, indicating
	// that there are no alternate notations.
	p = make([][]byte, 1, 4)
	p[0] = p0
	if !more {
		return
	}
	// In the event that we have alternate notations, we know from formatShortest
	// that p[0] is in the format {piece}(x){arrival file}{arrival rank}, where
	// 'x' may or may not be present. To create the alternate moves, we also need
	// to know the departure file and rank, which we capture in df and dr.
	df, dr := byte(m[0].File()), byte(m[0].Rank())
	if p0[1] == 'x' {
		p = append(p,
			[]byte{p0[0], df, p0[1], p0[2], p0[3]},
			[]byte{p0[0], dr, p0[1], p0[2], p0[3]},
			[]byte{p0[0], df, dr, p0[1], p0[2], p0[3]})
	} else {
		p = append(p,
			[]byte{p0[0], df, p0[1], p0[2]},
			[]byte{p0[0], dr, p0[1], p0[2]},
			[]byte{p0[0], df, dr, p0[1], p0[2]})
	}
	return
}

// Format returns the serialized version(s) of move m when formatted according
// to Standard Algebraic Notation given the board b. The returned slice s may
// have more than one element in the event alternate notations are possible to
// disambiguate moves. In this case, it is the responsibility of the calling
// code to choose the correct alternate.
//
// Generally you will not want to call this method from outside this package,
// as the Format method defined on the Moves type handles the task of
// disambiguating moves.
func (m Move) Format(b Board) (s []string) {
	p := m.formatAll(b)
	s = make([]string, len(p))
	for i := range s {
		s[i] = string(p[i])
	}
	return
}
