package chess

// MoveFinder describes the interface for an algorithm that can determine all
// valid moves given a position.
type MoveFinder interface {
	Moves(Game) Moves
	Attacked(Board, Square, Piece) bool
}
