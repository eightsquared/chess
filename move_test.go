package chess

import (
	"testing"
)

func TestMoveCaptures(t *testing.T) {
	board := Board{D5: WhitePawn, C5: BlackPawn, E6: BlackPawn}
	cases := []struct {
		title    string
		move     Move
		expected bool
	}{
		{"invalid move 1", Move{}, false},
		{"invalid move 2", Move{D5}, false},
		{"capture", Move{D5, E6}, true},
		{"no capture", Move{D5, D6}, false},
		{"capture en passant", Move{D5, C6, C5, None}, true},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			actual, expected := cases[i].move.Captures(board), cases[i].expected
			if actual != expected {
				t.Errorf("expected: %v; received: %v", expected, actual)
			}
		}
	}
	for i, c := range cases {
		t.Run(c.title, test(i))
	}
}

func TestMoveFormat(t *testing.T) {
	cases := []struct {
		title    string
		board    Board
		move     Move
		expected []string
	}{
		{"invalid move 1", Board{}, Move{}, []string{}},
		{"invalid move 2", Board{}, Move{D4}, []string{}},
		{"invalid move 3", Board{}, Move{None, D4}, []string{}},
		{"invalid move 4", Board{}, Move{D4, None}, []string{}},
		{"invalid move 5", Board{D4: WhitePawn}, Move{E4, D4}, []string{}},
		{"pawn", Board{D4: WhitePawn}, Move{D4, D5}, []string{"d5"}},
		{"pawn capture left", Board{D4: WhitePawn, C5: BlackPawn}, Move{D4, C5}, []string{"dxc5"}},
		{"pawn capture right", Board{D4: WhitePawn, E5: BlackPawn}, Move{D4, E5}, []string{"dxe5"}},
		{"pawn capture left en passant", Board{D5: WhitePawn, C5: BlackPawn}, Move{D5, C6, C5, None}, []string{"dxc6 e.p."}},
		{"pawn capture right en passant", Board{D5: WhitePawn, E5: BlackPawn}, Move{D5, E6, E5, None}, []string{"dxe6 e.p."}},
		{"pawn promote to knight", Board{D7: WhitePawn}, Move{D7, D8, ReplaceWithKnight, D8}, []string{"d8N"}},
		{"pawn promote to bishop", Board{D7: WhitePawn}, Move{D7, D8, ReplaceWithBishop, D8}, []string{"d8B"}},
		{"pawn promote to rook", Board{D7: WhitePawn}, Move{D7, D8, ReplaceWithRook, D8}, []string{"d8R"}},
		{"pawn promote to queen", Board{D7: WhitePawn}, Move{D7, D8, ReplaceWithQueen, D8}, []string{"d8Q"}},
		{"pawn capture and promote to knight", Board{D7: WhitePawn, C8: BlackQueen}, Move{D7, C8, ReplaceWithKnight, C8}, []string{"dxc8N"}},
		{"pawn capture and promote to bishop", Board{D7: WhitePawn, C8: BlackQueen}, Move{D7, C8, ReplaceWithBishop, C8}, []string{"dxc8B"}},
		{"pawn capture and promote to rook", Board{D7: WhitePawn, C8: BlackQueen}, Move{D7, C8, ReplaceWithRook, C8}, []string{"dxc8R"}},
		{"pawn capture and promote to queen", Board{D7: WhitePawn, C8: BlackQueen}, Move{D7, C8, ReplaceWithQueen, C8}, []string{"dxc8Q"}},
		{"knight", Board{D4: WhiteKnight}, Move{D4, B2}, []string{"Nb2", "Ndb2", "N4b2", "Nd4b2"}},
		{"knight capture", Board{D4: WhiteKnight, B2: BlackQueen}, Move{D4, B2}, []string{"Nxb2", "Ndxb2", "N4xb2", "Nd4xb2"}},
		{"bishop", Board{D4: WhiteBishop}, Move{D4, A1}, []string{"Ba1", "Bda1", "B4a1", "Bd4a1"}},
		{"bishop capture", Board{D4: WhiteBishop, A1: BlackQueen}, Move{D4, A1}, []string{"Bxa1", "Bdxa1", "B4xa1", "Bd4xa1"}},
		{"rook", Board{D4: WhiteRook}, Move{D4, A4}, []string{"Ra4", "Rda4", "R4a4", "Rd4a4"}},
		{"rook capture", Board{D4: WhiteRook, A4: BlackQueen}, Move{D4, A4}, []string{"Rxa4", "Rdxa4", "R4xa4", "Rd4xa4"}},
		{"queen", Board{D4: WhiteQueen}, Move{D4, D8}, []string{"Qd8", "Qdd8", "Q4d8", "Qd4d8"}},
		{"queen capture", Board{D4: WhiteQueen, D8: BlackQueen}, Move{D4, D8}, []string{"Qxd8", "Qdxd8", "Q4xd8", "Qd4xd8"}},
		{"king", Board{D4: WhiteKing}, Move{D4, E4}, []string{"Ke4", "Kde4", "K4e4", "Kd4e4"}},
		{"king capture", Board{D4: WhiteKing, E4: BlackQueen}, Move{D4, E4}, []string{"Kxe4", "Kdxe4", "K4xe4", "Kd4xe4"}},
		{"white kingside castle", Board{E1: WhiteKing, H1: WhiteRook}, Move{E1, G1, H1, F1}, []string{"0-0"}},
		{"black kingside castle", Board{E8: BlackKing, H8: BlackRook}, Move{E8, G8, H8, F8}, []string{"0-0"}},
		{"white queenside castle", Board{E1: WhiteKing, A1: WhiteRook}, Move{E1, C1, A1, D1}, []string{"0-0-0"}},
		{"black queenside castle", Board{E8: BlackKing, A8: BlackRook}, Move{E8, C8, A8, D8}, []string{"0-0-0"}},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			actual, expected := cases[i].move.Format(cases[i].board), cases[i].expected
			if len(actual) != len(expected) {
				t.Fatalf("format %v: expected: %v; received: %v", cases[i].move, expected, actual)
			}
			for i := range actual {
				if actual[i] != expected[i] {
					t.Errorf("format %v: expected: %v; received: %v", cases[i].move, expected, actual)
				}
			}
		}
	}
	for i, c := range cases {
		t.Run(c.title, test(i))
	}
}

func BenchmarkMoveFormat(b *testing.B) {
	cases := []struct {
		title string
		board Board
		move  Move
	}{
		{"pawn", Board{D4: WhitePawn}, Move{D4, D5}},
		{"pawn capture left", Board{D4: WhitePawn, C5: BlackPawn}, Move{D4, C5}},
		{"pawn capture right", Board{D4: WhitePawn, E5: BlackPawn}, Move{D4, E5}},
		{"pawn capture left en passant", Board{D5: WhitePawn, C5: BlackPawn}, Move{D5, C6, C5, None}},
		{"pawn capture right en passant", Board{D5: WhitePawn, E5: BlackPawn}, Move{D5, E6, E5, None}},
		{"pawn promote to knight", Board{D7: WhitePawn}, Move{D7, D8, ReplaceWithKnight, D8}},
		{"pawn capture and promote to knight", Board{D7: WhitePawn, C8: BlackQueen}, Move{D7, C8, ReplaceWithKnight, C8}},
		{"knight", Board{D4: WhiteKnight}, Move{D4, B2}},
		{"white kingside castle", Board{E1: WhiteKing, H1: WhiteRook}, Move{E1, G1, H1, F1}},
		{"white queenside castle", Board{E1: WhiteKing, A1: WhiteRook}, Move{E1, C1, A1, D1}},
	}
	b.Run("random", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			j := i % len(cases)
			cases[j].move.Format(cases[j].board)
		}
	})
}
