package chess

// Moves represents a set of individual moves. This could be a set of valid
// moves for a given position, or a subset of those moves.
type Moves []Move

// Equals determines whether the set of moves ms is equal to the given set of
// moves ns. Two sets of moves are considered equal if they are of the same
// length and if the corresponding individual moves are all equal.
func (ms Moves) Equals(ns Moves) (equal bool) {
	if len(ms) != len(ns) {
		return
	}
	for _, m := range ms {
		found := false
		for _, n := range ns {
			if m.Equals(n) {
				found = true
				break
			}
		}
		if !found {
			return
		}
	}
	equal = true
	return
}

// Find attempts to find a move in ms that matches the given Standard Algebraic
// Notation san when the moves are formatted by passing the board b to the
// Format method. If a match is found, ok is set to true.
//
// This method uses a linear search to locate the move.
func (ms Moves) Find(b Board, san string) (m Move, ok bool) {
	ss := ms.Format(b)
	for i, s := range ss {
		if s == san {
			m = ms[i]
			ok = true
			return
		}
	}
	return
}

// Format returns a slice of strings ss equal in length to the set of moves ms,
// where each element in ss has the shortest possible serialized version of the
// corresponding element in ms when that element is formatted according to
// Standard Algebraic Notation given the board b. This method assumes that ms
// contains the set of all valid moves for the given board state and uses only
// information in ms to disambiguate moves with the same short notation. This
// method also assumes that there are no duplicates in ms and that there are no
// more than 16 moves sharing an arrival square. Violating these assumptions
// may cause this method to panic or return unexpected results.
func (ms Moves) Format(b Board) (ss []string) {
	// The general flow of this function is as follows:
	//
	// 1. Loop through all squares.
	// 2. For each square, find all moves that arrive at the square.
	//    (early continue in case of zero or one move)
	// 3. If there is more than one such move, loop through all piece classes.
	// 4. For each piece class, find all moves that arrive at the square.
	//    (early continue in case of zero or one move)
	// 5. If there is more than one such move, choose the shortest possible
	//    notation from all candidates.
	//
	// This function has been optimized by avoiding allocations (especially
	// silent allocations through append) where possible and by providing early
	// exits to avoid expensive data structures such as maps unless absolutely
	// necessary.
	//
	// We already know the length of the return slice, so we can allocate it
	// immediately.
	ss = make([]string, len(ms))
	// We can avoid repeated allocations by creating "scratch space" that is
	// reused across iterations. m2s will hold all moves to the square under
	// consideration, while pm2s will hold all moves by the piece class and to
	// the square under consideration. Assuming we are following standard chess
	// rules, we know that there can be no more than 16 moves arriving at the
	// same square (imagine a square surrounded by eight queens and eight
	// knights), and no more than eight moves by any one piece class arriving at
	// the same square. This provides an upper bound for the scratch space
	// length.
	m2s := make(Moves, 16)
	pm2s := make(Moves, 8)
	// We will need to connect the moves in m2s and pm2s back to the original
	// elements in ms, so m2si and pm2si will hold the indexes into ms. That is,
	// the ith element of m2si (or pm2si) will hold the index j in ms that
	// matches the ith element in m2s (or pm2s). This allows us to set the
	// correct value in ss.
	m2si := make([]int, 16)
	pm2si := make([]int, 8)
	// Preallocating the scratch space requires us to track the length manually.
	var m2slen, pm2slen int
	for sq := First; sq <= Last; sq++ {
		// Find all moves that arrive at sq. For simplicity, we assume that a move
		// arrives at sq if the second square in the move equals sq.
		m2slen = 0
		for i, m := range ms {
			if len(m) >= 2 && m[1] == sq {
				m2s[m2slen] = m
				m2si[m2slen] = i
				m2slen++
			}
		}
		switch m2slen {
		case 0:
			// No moves arrive at sq; no work to do.
			continue
		case 1:
			// Exactly one move arrives at sq, so it can't be confused with any other
			// move. The shortest possible notation will suffice.
			s, _ := m2s[0].formatShortest(b)
			ss[m2si[0]] = string(s)
			continue
		default:
			// More than one move arrives at sq. Now we break down the moves in m2s
			// according to piece class, as only moves by the same piece class could
			// have conflicting notation.
			for _, p := range pieceClasses {
				pm2slen = 0
				for i := 0; i < m2slen; i++ {
					m := m2s[i]
					if b[m[0]].Is(p) {
						pm2s[pm2slen] = m
						pm2si[pm2slen] = m2si[i]
						pm2slen++
					}
				}
				switch pm2slen {
				case 0:
					// No moves by a piece of class p arrive at sq; continue with the
					// next piece class.
					continue
				case 1:
					// Exactly one move by a piece of class p arrives at sq, so the
					// shortest possible notation is guaranteed to be unique.
					s, _ := pm2s[0].formatShortest(b)
					ss[pm2si[0]] = string(s)
					continue
				default:
					// By virtue of arriving here we know that at least two pieces of the
					// same class have the same arrival square. For this rare case we
					// pull out a map to keep track of which move notations have been
					// seen exactly once. For each move in pm2s, we go through all
					// possible notations. If we have seen the notation before (if it is
					// present in fmti), we set fmti[s] to -1, signaling that this
					// notation cannot be used because it is ambiguous. Otherwise we use
					// fmti[s] to store the index of the move in ms (obtained indirectly
					// from pm2si).
					fmti := make(map[string]int)
					var ms [][]byte
					var seen bool
					for i := 0; i < pm2slen; i++ {
						ms = pm2s[i].formatAll(b)
						for j := 0; j < len(ms); j++ {
							s := string(ms[j])
							if _, seen = fmti[s]; seen {
								fmti[s] = -1
							} else {
								fmti[s] = pm2si[i]
							}
						}
					}
					// Now we can loop through all possible notations in fmti, ignoring
					// the ambiguous ones (where i is less than zero), and overwriting
					// ss[i] if we find that a) ss[i] has not yet been set, b) the
					// notation in fmti is shorter than what is in ss[i], or c) the
					// notation in fmti is the same length as what is in ss[i], but the
					// second byte of the former is greater than the second byte of the
					// latter. (The third case covers disambiguating between moves like
					// Nbc3 and N1c3; according to Standard Algebraic Notation, the file
					// should be used to distinguish between moves prior to the rank.)
					for s, i := range fmti {
						if i < 0 {
							continue
						}
						if ss[i] == "" || len(s) < len(ss[i]) || (len(s) == len(ss[i]) && s[1] > ss[i][1]) {
							ss[i] = s
						}
					}
				}
			}
		}
	}
	return ss
}

// From returns a filtered set of moves that contains all moves in ms that
// depart from the given square s. A move m is considered to depart from s if
// its first square equals s.
func (ms Moves) From(s Square) (ns Moves) {
	ns = make(Moves, 0)
	for _, m := range ms {
		if len(m) > 0 && m[0] == s {
			ns = append(ns, m)
		}
	}
	return
}

// Has determines whether the set of moves ms contains the given move n at any
// index.
func (ms Moves) Has(n Move) (has bool) {
	for _, m := range ms {
		if m.Equals(n) {
			has = true
			return
		}
	}
	return
}

// Len satisfies the sort package's Interface.
func (ms Moves) Len() int {
	return len(ms)
}

// Less satisfies the sort package's Interface.
func (ms Moves) Less(i, j int) (less bool) {
	for k := 0; k < len(ms[i]) && k < len(ms[j]); k++ {
		if ms[i][k] < ms[j][k] {
			less = true
			return
		}
		if ms[i][k] > ms[j][k] {
			return
		}
	}
	if len(ms[i]) < len(ms[j]) {
		less = true
	}
	return
}

// Swap satisfies the sort package's Interface.
func (ms Moves) Swap(i, j int) {
	ms[i], ms[j] = ms[j], ms[i]
}
