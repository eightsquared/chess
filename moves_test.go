package chess

import (
	"fmt"
	"testing"
)

func TestMovesEquals(t *testing.T) {
	t.Parallel()
	moves := Moves{Move{A1, B2}, Move{A1, C3}}
	cases := []struct {
		moves    Moves
		expected bool
	}{
		{Moves{}, false},
		{Moves{Move{A1, B2}}, false},
		{Moves{Move{A1, C3}}, false},
		{Moves{Move{A1, B2}, Move{A1, C3}}, true},
		{Moves{Move{A1, B2}, Move{A1, D4}}, false},
		{Moves{Move{A1, B2}, Move{A1, C3}, Move{A1, D4}}, false},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			actual, expected := moves.Equals(cases[i].moves), cases[i].expected
			if actual != expected {
				t.Errorf("expected %v; received %v", expected, actual)
			}
		}
	}
	for i, c := range cases {
		t.Run(fmt.Sprintf("%v", c.moves), test(i))
	}
}

func TestMovesFind(t *testing.T) {
	t.Parallel()
	board := Board{A1: WhiteKing}
	moves := Moves{Move{A1, A2}, Move{A1, B2}, Move{A1, B1}}
	if m, ok := moves.Find(board, "Ka2"); m == nil || !m.Equals(moves[0]) || !ok {
		t.Fatalf("%s: expected: %v, %v; received: %v, %v", "Ka2", moves[0], true, m, ok)
	}
	if m, ok := moves.Find(board, "Kb2"); m == nil || !m.Equals(moves[1]) || !ok {
		t.Fatalf("%s: expected: %v, %v; received: %v, %v", "Kb2", moves[1], true, m, ok)
	}
	if m, ok := moves.Find(board, "Kb1"); m == nil || !m.Equals(moves[2]) || !ok {
		t.Fatalf("%s: expected: %v, %v; received: %v, %v", "Kb1", moves[2], true, m, ok)
	}
	if m, ok := moves.Find(board, "Ka8"); m != nil || ok {
		t.Fatalf("%s: expected: %v, %v; received: %v, %v", "Ka8", nil, false, m, ok)
	}
}

func TestMovesFormat(t *testing.T) {
	t.Parallel()
	cases := []struct {
		title    string
		board    Board
		moves    Moves
		expected []string
	}{
		{"no moves", Board{}, Moves{}, []string{}},
		{
			"one move",
			Board{A2: WhiteQueen},
			Moves{Move{A2, A1}},
			[]string{"Qa1"},
		},
		{
			"two queens on same rank",
			Board{A2: WhiteQueen, B2: WhiteQueen},
			Moves{Move{A2, A1}, Move{B2, A1}},
			[]string{"Qaa1", "Qba1"},
		},
		{
			"two queens on same file",
			Board{B2: WhiteQueen, B1: WhiteQueen},
			Moves{Move{B2, A1}, Move{B1, A1}},
			[]string{"Q2a1", "Q1a1"},
		},
		{
			"three queens to same square",
			Board{A2: WhiteQueen, B2: WhiteQueen, B1: WhiteQueen},
			Moves{Move{A2, A1}, Move{B2, A1}, Move{B1, A1}},
			[]string{"Qaa1", "Qb2a1", "Q1a1"},
		},
		{
			"four queens to same square",
			Board{A1: WhiteQueen, A2: WhiteQueen, C2: WhiteQueen, C1: WhiteQueen},
			Moves{Move{A1, B1}, Move{A2, B1}, Move{C2, B1}, Move{C1, B1}},
			[]string{"Qa1b1", "Qa2b1", "Qc2b1", "Qc1b1"},
		},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			actual, expected := cases[i].moves.Format(cases[i].board), cases[i].expected
			if len(actual) != len(expected) {
				t.Fatalf("%v: expected: %v; received: %v", cases[i].moves, expected, actual)
			}
			for j := range actual {
				if actual[j] != expected[j] {
					t.Fatalf("%v: expected: %v; received: %v", cases[i].moves, expected, actual)
				}
			}
		}
	}
	for i, c := range cases {
		t.Run(c.title, test(i))
	}
}

func BenchmarkMovesFormat(b *testing.B) {
	const n = 10000
	strategy := new(BitwiseMoveFinder)
	moves := make([]Moves, n)
	boards := make([]Board, n)
	for i := range moves {
		g := RandomGame(i)
		moves[i], boards[i] = strategy.Moves(g), g.Board
	}
	b.Run("random", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			j := i % len(moves)
			moves[j].Format(boards[j])
		}
	})
}

func BenchmarkMovesFrom(b *testing.B) {
	const n = 10000
	strategy := new(BitwiseMoveFinder)
	moves := make([]Moves, n)
	for i := range moves {
		moves[i] = strategy.Moves(RandomGame(i))
	}
	b.Run("random", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			j := i % len(moves)
			moves[j].From(D4)
		}
	})
}

func TestMovesHas(t *testing.T) {
	t.Parallel()
	moves := Moves{Move{A1, B2}, Move{A1, C3}, Move{A1, D4}}
	cases := []struct {
		move     Move
		expected bool
	}{
		{Move{A1, B2}, true},
		{Move{A1, C3}, true},
		{Move{A1, D4}, true},
		{Move{A1, E5}, false},
		{Move{B2, A1}, false},
		{Move{C3, A1}, false},
		{Move{D4, A1}, false},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			actual, expected := moves.Has(cases[i].move), cases[i].expected
			if actual != expected {
				t.Errorf("moves has %v: expected %v; received %v", cases[i].move, expected, actual)
			}
		}
	}
	for i, c := range cases {
		t.Run(fmt.Sprintf("%v", c.move), test(i))
	}
}

func TestMovesLen(t *testing.T) {
	t.Parallel()
	cases := []struct {
		moves    Moves
		expected int
	}{
		{Moves{}, 0},
		{Moves{Move{A1, C3}}, 1},
		{Moves{Move{A1, C3}, Move{A1, D4}}, 2},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			actual, expected := cases[i].moves.Len(), cases[i].expected
			if actual != expected {
				t.Errorf("expected %v; received %v", expected, actual)
			}
		}
	}
	for i, c := range cases {
		t.Run(fmt.Sprintf("%v", c.moves), test(i))
	}
}

func TestMovesLess(t *testing.T) {
	t.Parallel()
	cases := []struct {
		title    string
		moves    Moves
		expected bool
	}{
		{"same length; 1.0 less", Moves{Move{A1, A2}, Move{B1, A2}}, true},
		{"same length; 0.0 less", Moves{Move{A1, A2}, Move{A2, A1}}, false},
		{"same length; 0.1 less", Moves{Move{A1, A2}, Move{A1, B2}}, true},
		{"same length; 1.1 less", Moves{Move{A1, B2}, Move{A1, A2}}, false},
		{"different length; move 0 longer", Moves{Move{A1, A2, A2, B2}, Move{A1, A2}}, false},
		{"different length; move 1 longer", Moves{Move{A1, A2}, Move{A1, A2, A2, B2}}, true},
	}
	test := func(i int) func(*testing.T) {
		return func(t *testing.T) {
			t.Parallel()
			actual, expected := cases[i].moves.Less(0, 1), cases[i].expected
			if actual != expected {
				t.Errorf("expected %v; received %v", expected, actual)
			}
		}
	}
	for i, c := range cases {
		t.Run(c.title, test(i))
	}
}

func TestMovesSwap(t *testing.T) {
	t.Parallel()
	a1a2, a1b2 := Move{A1, A2}, Move{A1, B2}
	moves := Moves{a1a2, a1b2}
	moves.Swap(0, 1)
	if actual, expected := moves[0], a1b2; !actual.Equals(expected) {
		t.Errorf("expected %v; received %v", expected, actual)
	}
	if actual, expected := moves[1], a1a2; !actual.Equals(expected) {
		t.Errorf("expected %v; received %v", expected, actual)
	}
}
