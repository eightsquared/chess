package chess

// Offset can be used to represent the difference between two squares on the
// board, or a vector from a starting square. An offset is expressed as a pair
// of integers, where Files represents the horizontal component (increasing
// toward the right edge of the board) and Ranks represents the vertical
// component (increasing toward the top edge of the board).
type Offset struct {
	Files, Ranks int8
}

// Unit returns a new offset p in which all values in offset o that are greater
// than 1 or less than −1 have been converted to 1 and −1, respectively.
func (o Offset) Unit() (p Offset) {
	p = o
	if p.Files > 1 {
		p.Files = 1
	} else if p.Files < -1 {
		p.Files = -1
	}
	if p.Ranks > 1 {
		p.Ranks = 1
	} else if p.Ranks < -1 {
		p.Ranks = -1
	}
	return
}

// Abs returns a new offset p in which contains the absolute value of all
// values in offset o.
func (o Offset) Abs() (p Offset) {
	p = o
	if p.Files < 0 {
		p.Files *= -1
	}
	if p.Ranks < 0 {
		p.Ranks *= -1
	}
	return
}
