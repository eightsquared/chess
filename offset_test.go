package chess

import (
	"testing"
)

func TestOffsetAbs(t *testing.T) {
	t.Parallel()
	cases := [][2]Offset{
		{{0, 0}, {0, 0}},
		{{4, 4}, {4, 4}},
		{{4, -4}, {4, 4}},
		{{-4, 4}, {4, 4}},
		{{-4, -4}, {4, 4}},
	}
	for _, pair := range cases {
		actual, expected := pair[0].Abs(), pair[1]
		if actual.Files != expected.Files || actual.Ranks != expected.Ranks {
			t.Errorf("abs %v: expected %v; received %v", pair[0], expected, actual)
		}
	}
}

func TestOffsetUnit(t *testing.T) {
	t.Parallel()
	cases := [][2]Offset{
		{{0, 0}, {0, 0}},
		{{4, 4}, {1, 1}},
		{{4, -4}, {1, -1}},
		{{-4, 4}, {-1, 1}},
		{{-4, -4}, {-1, -1}},
	}
	for _, pair := range cases {
		actual, expected := pair[0].Unit(), pair[1]
		if actual.Files != expected.Files || actual.Ranks != expected.Ranks {
			t.Errorf("unit %v: expected %v; received %v", pair[0], expected, actual)
		}
	}
}
