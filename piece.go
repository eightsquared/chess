package chess

// Piece represents a distinct combination of piece class (such as pawn or
// knight) and color (white or black).
type Piece uint8

// Empty is a convenient alias for the default value of Piece and represents an
// empty square.
const Empty Piece = 0

// Since there are two possible colors and six piece classes in a standard game
// of chess, we can use the two least significant bits to represent the color
// and the six remaining bits to represent the class. This is admittedly not
// the most efficient way to use a byte, as the 12 distinct values could be
// represented with just four bits, but it does make some common operations and
// tests trivial through bitwise math.
const (
	White Piece = 1 << iota
	Black
	Pawn
	Knight
	Bishop
	Rook
	Queen
	King
)

// For convenience in describing positions in code, we provide named constants
// for all 12 chess pieces.
const (
	WhitePawn   Piece = White | Pawn
	WhiteKnight Piece = White | Knight
	WhiteBishop Piece = White | Bishop
	WhiteRook   Piece = White | Rook
	WhiteQueen  Piece = White | Queen
	WhiteKing   Piece = White | King
	BlackPawn   Piece = Black | Pawn
	BlackKnight Piece = Black | Knight
	BlackBishop Piece = Black | Bishop
	BlackRook   Piece = Black | Rook
	BlackQueen  Piece = Black | Queen
	BlackKing   Piece = Black | King
)

// pieceRunes maps piece constants to runes according to the convention set in
// Forsyth–Edwards Notation.
var pieceRunes = [131]rune{
	Empty:       ' ',
	White:       'w',
	Black:       'b',
	WhitePawn:   'P',
	WhiteKnight: 'N',
	WhiteBishop: 'B',
	WhiteRook:   'R',
	WhiteQueen:  'Q',
	WhiteKing:   'K',
	BlackPawn:   'p',
	BlackKnight: 'n',
	BlackBishop: 'b',
	BlackRook:   'r',
	BlackQueen:  'q',
	BlackKing:   'k',
}

// pieceClasses provides a slice of piece constants for functions that need to
// iterate through all classes without regard for color.
var pieceClasses = []Piece{
	Pawn,
	Knight,
	Bishop,
	Rook,
	Queen,
	King,
}

// Color returns the color component of the piece p.
func (p Piece) Color() Piece {
	return p & (White | Black)
}

// Invert returns a piece that is the same class as the piece p but the
// opposite color.
func (p Piece) Invert() Piece {
	return p ^ (White | Black)
}

// Is determines whether the piece p is the given piece q. This is not a strict
// equality test; rather, q is used as a mask against p and the resulting value
// is compared to q. This allows you to pass values such as White or Pawn to
// this method and determine whether p is any white piece or is a pawn of any
// color. Due to the simplicity of this operation, the body of this method is
// inlined by the compiler, making it very cheap to call.
func (p Piece) Is(q Piece) bool {
	return p&q == q
}

// Rune returns a single character that represents the piece p. If there is no
// known rune for the piece, this method returns '?'.
func (p Piece) Rune() (r rune) {
	r = '?'
	if i := int(p); i >= 0 && i < len(pieceRunes) {
		r = pieceRunes[i]
	}
	return
}
