package chess

import (
	"testing"
)

func TestPieceRune(t *testing.T) {
	t.Parallel()
	cases := []struct {
		piece    Piece
		expected rune
	}{
		{WhitePawn, 'P'},
		{WhiteKnight, 'N'},
		{WhiteBishop, 'B'},
		{WhiteRook, 'R'},
		{WhiteQueen, 'Q'},
		{WhiteKing, 'K'},
		{BlackPawn, 'p'},
		{BlackKnight, 'n'},
		{BlackBishop, 'b'},
		{BlackRook, 'r'},
		{BlackQueen, 'q'},
		{BlackKing, 'k'},
	}
	for _, c := range cases {
		if actual := c.piece.Rune(); actual != c.expected {
			t.Errorf("rune for %v: expected %c; received %c", c.piece, c.expected, actual)
		}
	}
}

func TestPieceColor(t *testing.T) {
	t.Parallel()
	cases := []struct {
		piece    Piece
		expected Piece
	}{
		{White, White},
		{WhitePawn, White},
		{WhiteKnight, White},
		{WhiteBishop, White},
		{WhiteRook, White},
		{WhiteQueen, White},
		{WhiteKing, White},
		{Black, Black},
		{BlackPawn, Black},
		{BlackKnight, Black},
		{BlackBishop, Black},
		{BlackRook, Black},
		{BlackQueen, Black},
		{BlackKing, Black},
	}
	for _, c := range cases {
		if actual := c.piece.Color(); actual != c.expected {
			t.Errorf("color for %v: expected %c; received %c", c.piece, c.expected, actual)
		}
	}
}

func TestPieceInvert(t *testing.T) {
	t.Parallel()
	cases := []struct {
		piece    Piece
		expected Piece
	}{
		{White, Black},
		{WhitePawn, BlackPawn},
		{WhiteKnight, BlackKnight},
		{WhiteBishop, BlackBishop},
		{WhiteRook, BlackRook},
		{WhiteQueen, BlackQueen},
		{WhiteKing, BlackKing},
		{Black, White},
		{BlackPawn, WhitePawn},
		{BlackKnight, WhiteKnight},
		{BlackBishop, WhiteBishop},
		{BlackRook, WhiteRook},
		{BlackQueen, WhiteQueen},
		{BlackKing, WhiteKing},
	}
	for _, c := range cases {
		if actual := c.piece.Invert(); actual != c.expected {
			t.Errorf("flipped color for %v: expected %c; received %c", c.piece, c.expected, actual)
		}
	}
}

func TestPieceIs(t *testing.T) {
	t.Parallel()
	cases := []struct {
		piece    Piece
		kind     Piece
		expected bool
	}{
		{WhitePawn, White, true},
		{WhitePawn, Pawn, true},
		{WhitePawn, WhitePawn, true},
		{WhitePawn, WhiteKnight, false},
		{WhitePawn, Black, false},
		{WhitePawn, BlackPawn, false},
		{BlackPawn, Black, true},
		{BlackPawn, Pawn, true},
		{BlackPawn, BlackPawn, true},
		{BlackPawn, BlackKnight, false},
		{BlackPawn, White, false},
		{BlackPawn, WhitePawn, false},
	}
	for _, c := range cases {
		if actual := c.piece.Is(c.kind); actual != c.expected {
			t.Errorf("%v is %v? expected %v; received %v", c.piece, c.kind, c.expected, actual)
		}
	}
}
