package chess

// Square represents a location on a chess board.
type Square int8

// For convenience, we provide named versions of all valid squares, starting
// from the top left square (a8) according to white's point of view and
// proceeding rank-by-rank until reaching the bottom right square (h1). (Go's
// convention of requiring package exports to begin with an upper case letter
// requires us to use names like A8 and H1 rather than the lower case versions
// used in chess literature.)
//
// Combined with the happy coincidence that a chess board has eight squares on
// each side, listing the squares in this order means that a square's file
// (expressed as a zero-based index) is encoded in the three least significant
// bits, while the rank is expressed by the three next most significant bits.
// (Since we start listing from the eighth rank, however, we must take care to
// subtract the value of the rank bits from seven in order to obtain a
// zero-based rank index.)
const (
	A8 Square = iota
	B8
	C8
	D8
	E8
	F8
	G8
	H8
	A7
	B7
	C7
	D7
	E7
	F7
	G7
	H7
	A6
	B6
	C6
	D6
	E6
	F6
	G6
	H6
	A5
	B5
	C5
	D5
	E5
	F5
	G5
	H5
	A4
	B4
	C4
	D4
	E4
	F4
	G4
	H4
	A3
	B3
	C3
	D3
	E3
	F3
	G3
	H3
	A2
	B2
	C2
	D2
	E2
	F2
	G2
	H2
	A1
	B1
	C1
	D1
	E1
	F1
	G1
	H1
)

// When looping through all squares in the board, it is convenient to have
// aliases for A8 and H1 so that we need not remember from which square we must
// start and on which square we must end.
const (
	First Square = A8
	Last  Square = H1
)

// Since Square is defined as a signed integer, we can take advantage of
// negative values to express special concepts. The first of these is None,
// which is often used as a return value in movement methods to indicate that
// the given movement results in a position beyond the boundaries of the board.
//
const (
	None Square = -1
)

// We also have special squares used in pawn moves that result in a promotion
// to indicate which of the four possible promotion pieces replaces the pawn.
const (
	ReplaceWithKnight Square = -2
	ReplaceWithBishop Square = -3
	ReplaceWithRook   Square = -4
	ReplaceWithQueen  Square = -5
)

// Valid returns true if the square s represents a valid location on the board
// (that is, s is between A8 and H1, inclusively).
func (s Square) Valid() bool {
	return s >= First && s <= Last
}

// Replacement returns the piece r that will replace the given piece p
// according to the value of the square s. This method can only return a value
// not equal to p if s is one of the special "ReplaceWith" constants defined
// above. The returned piece r will always be the same color as p.
func (s Square) Replacement(p Piece) (r Piece) {
	r = p
	switch s {
	case ReplaceWithKnight:
		r = Knight | p.Color()
	case ReplaceWithBishop:
		r = Bishop | p.Color()
	case ReplaceWithRook:
		r = Rook | p.Color()
	case ReplaceWithQueen:
		r = Queen | p.Color()
	}
	return
}

// Light returns true if the square s should be given a light color when
// rendering an image of the board.
func (s Square) Light() bool {
	if s.Valid() {
		return ((int(s)&7)+(int(s)>>3))&1 == 0
	}
	return false
}

// File returns a single character denoting the column of the square s.  If s
// is on the board, the character will be in the range 'a' to 'h' (inclusive).
// Any other value of s results in '?'.
func (s Square) File() rune {
	if s.Valid() {
		return 'a' + rune(int(s)&7)
	}
	return '?'
}

// Rank returns a single character denoting the row of the square s.  If s is
// on the board, the character will be in the range '1' to '8' (inclusive). Any
// other value of s results in '?'.
func (s Square) Rank() rune {
	if s.Valid() {
		return '1' + rune(7-(int(s)>>3))
	}
	return '?'
}

// Up returns the square that is to the north of the square s. If s is part of
// the topmost rank, this method returns None.
func (s Square) Up() Square {
	if s.Valid() && int(s)>>3 > 0 {
		return s - 8
	}
	return None
}

// Down returns the square that is to the south of the square s. If s is part
// of the bottommost rank, this method returns None.
func (s Square) Down() Square {
	if s.Valid() && int(s)>>3 < 7 {
		return s + 8
	}
	return None
}

// Left returns the square that is to the west of the square s. If s is part of
// the leftmost rank, this method returns None.
func (s Square) Left() Square {
	if s.Valid() && int(s)&7 > 0 {
		return s - 1
	}
	return None
}

// Right returns the square that is to the east of the square s. If s is part
// of the rightmost rank, this method returns None.
func (s Square) Right() Square {
	if s.Valid() && int(s)&7 < 7 {
		return s + 1
	}
	return None
}

// Forward returns either the square that is to the north or to the south of
// the square s depending on the color of the given piece p. If p is white, the
// square to the north is returned. Otherwise, if p is black, the square to the
// south is returned. If p is neither white nor black, this method returns
// None.
func (s Square) Forward(p Piece) Square {
	if s.Valid() {
		if p.Is(White) {
			return s.Up()
		} else if p.Is(Black) {
			return s.Down()
		}
	}
	return s
}

// Add returns the square that is offset from the square s by the given offset
// o. If applying the offset results in a square that is beyond the boundaries
// of the board, this method returns None.
func (s Square) Add(o Offset) Square {
	if s.Valid() {
		sr, sf := int8(s)>>3, int8(s)&7
		if tr, tf := sr-o.Ranks, sf+o.Files; tr >= 0 && tr < 8 && tf >= 0 && tf < 8 {
			return Square((tr << 3) | tf)
		}
	}
	return None
}

// Sub returns the offset o that, when added to the given square t, produces
// the square s.
func (s Square) Sub(t Square) (o Offset) {
	if s.Valid() && t.Valid() {
		o.Files = int8((s & 7) - (t & 7))
		o.Ranks = int8((t >> 3) - (s >> 3))
	}
	return
}

// String converts the square s to the standard text representation found in
// chess literature.
func (s Square) String() string {
	const squareStr = "a8b8c8d8e8f8g8h8a7b7c7d7e7f7g7h7a6b6c6d6e6f6g6h6a5b5c5d5e5f5g5h5a4b4c4d4e4f4g4h4a3b3c3d3e3f3g3h3a2b2c2d2e2f2g2h2a1b1c1d1e1f1g1h1"
	if i := int(s); s.Valid() {
		return squareStr[2*i : 2*i+2]
	}
	return "??"
}
