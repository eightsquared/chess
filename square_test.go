package chess

import (
	"testing"
)

func TestSquareValid(t *testing.T) {
	t.Parallel()
	cases := map[Square]bool{
		A1:   true,
		H8:   true,
		None: false,
	}
	for square, expected := range cases {
		if actual := square.Valid(); actual != expected {
			t.Errorf("valid for %s: expected %v; received %v", square, expected, actual)
		}
	}
}

func TestSquareLight(t *testing.T) {
	t.Parallel()
	cases := map[Square]bool{
		A1: false, A2: true, A3: false, A4: true, A5: false, A6: true, A7: false, A8: true,
		B1: true, B2: false, B3: true, B4: false, B5: true, B6: false, B7: true, B8: false,
		C1: false, C2: true, C3: false, C4: true, C5: false, C6: true, C7: false, C8: true,
		D1: true, D2: false, D3: true, D4: false, D5: true, D6: false, D7: true, D8: false,
		E1: false, E2: true, E3: false, E4: true, E5: false, E6: true, E7: false, E8: true,
		F1: true, F2: false, F3: true, F4: false, F5: true, F6: false, F7: true, F8: false,
		G1: false, G2: true, G3: false, G4: true, G5: false, G6: true, G7: false, G8: true,
		H1: true, H2: false, H3: true, H4: false, H5: true, H6: false, H7: true, H8: false,
		None: false,
	}
	for square, expected := range cases {
		if actual := square.Light(); actual != expected {
			t.Errorf("light for %s: expected %v; received %v", square, expected, actual)
		}
	}
}

func TestSquareFile(t *testing.T) {
	t.Parallel()
	cases := map[Square]rune{
		A1: 'a', A2: 'a', A3: 'a', A4: 'a', A5: 'a', A6: 'a', A7: 'a', A8: 'a',
		B1: 'b', B2: 'b', B3: 'b', B4: 'b', B5: 'b', B6: 'b', B7: 'b', B8: 'b',
		C1: 'c', C2: 'c', C3: 'c', C4: 'c', C5: 'c', C6: 'c', C7: 'c', C8: 'c',
		D1: 'd', D2: 'd', D3: 'd', D4: 'd', D5: 'd', D6: 'd', D7: 'd', D8: 'd',
		E1: 'e', E2: 'e', E3: 'e', E4: 'e', E5: 'e', E6: 'e', E7: 'e', E8: 'e',
		F1: 'f', F2: 'f', F3: 'f', F4: 'f', F5: 'f', F6: 'f', F7: 'f', F8: 'f',
		G1: 'g', G2: 'g', G3: 'g', G4: 'g', G5: 'g', G6: 'g', G7: 'g', G8: 'g',
		H1: 'h', H2: 'h', H3: 'h', H4: 'h', H5: 'h', H6: 'h', H7: 'h', H8: 'h',
		None: '?',
	}
	for square, expected := range cases {
		if actual := square.File(); actual != expected {
			t.Errorf("file for %s: expected %c; received %c", square, expected, actual)
		}
	}
}

func TestSquareRank(t *testing.T) {
	t.Parallel()
	cases := map[Square]rune{
		A1: '1', A2: '2', A3: '3', A4: '4', A5: '5', A6: '6', A7: '7', A8: '8',
		B1: '1', B2: '2', B3: '3', B4: '4', B5: '5', B6: '6', B7: '7', B8: '8',
		C1: '1', C2: '2', C3: '3', C4: '4', C5: '5', C6: '6', C7: '7', C8: '8',
		D1: '1', D2: '2', D3: '3', D4: '4', D5: '5', D6: '6', D7: '7', D8: '8',
		E1: '1', E2: '2', E3: '3', E4: '4', E5: '5', E6: '6', E7: '7', E8: '8',
		F1: '1', F2: '2', F3: '3', F4: '4', F5: '5', F6: '6', F7: '7', F8: '8',
		G1: '1', G2: '2', G3: '3', G4: '4', G5: '5', G6: '6', G7: '7', G8: '8',
		H1: '1', H2: '2', H3: '3', H4: '4', H5: '5', H6: '6', H7: '7', H8: '8',
		None: '?',
	}
	for square, expected := range cases {
		if actual := square.Rank(); actual != expected {
			t.Errorf("rank for %s: expected %c; received %c", square, expected, actual)
		}
	}
}

func TestSquareUp(t *testing.T) {
	t.Parallel()
	cases := map[Square]Square{
		A1: A2, A2: A3, A3: A4, A4: A5, A5: A6, A6: A7, A7: A8, A8: None,
		B1: B2, B2: B3, B3: B4, B4: B5, B5: B6, B6: B7, B7: B8, B8: None,
		C1: C2, C2: C3, C3: C4, C4: C5, C5: C6, C6: C7, C7: C8, C8: None,
		D1: D2, D2: D3, D3: D4, D4: D5, D5: D6, D6: D7, D7: D8, D8: None,
		E1: E2, E2: E3, E3: E4, E4: E5, E5: E6, E6: E7, E7: E8, E8: None,
		F1: F2, F2: F3, F3: F4, F4: F5, F5: F6, F6: F7, F7: F8, F8: None,
		G1: G2, G2: G3, G3: G4, G4: G5, G5: G6, G6: G7, G7: G8, G8: None,
		H1: H2, H2: H3, H3: H4, H4: H5, H5: H6, H6: H7, H7: H8, H8: None,
		None: None,
	}
	for square, expected := range cases {
		if actual := square.Up(); actual != expected {
			t.Errorf("up for %s: expected %c; received %c", square, expected, actual)
		}
	}
}

func TestSquareDown(t *testing.T) {
	t.Parallel()
	cases := map[Square]Square{
		A1: None, A2: A1, A3: A2, A4: A3, A5: A4, A6: A5, A7: A6, A8: A7,
		B1: None, B2: B1, B3: B2, B4: B3, B5: B4, B6: B5, B7: B6, B8: B7,
		C1: None, C2: C1, C3: C2, C4: C3, C5: C4, C6: C5, C7: C6, C8: C7,
		D1: None, D2: D1, D3: D2, D4: D3, D5: D4, D6: D5, D7: D6, D8: D7,
		E1: None, E2: E1, E3: E2, E4: E3, E5: E4, E6: E5, E7: E6, E8: E7,
		F1: None, F2: F1, F3: F2, F4: F3, F5: F4, F6: F5, F7: F6, F8: F7,
		G1: None, G2: G1, G3: G2, G4: G3, G5: G4, G6: G5, G7: G6, G8: G7,
		H1: None, H2: H1, H3: H2, H4: H3, H5: H4, H6: H5, H7: H6, H8: H7,
		None: None,
	}
	for square, expected := range cases {
		if actual := square.Down(); actual != expected {
			t.Errorf("down for %s: expected %c; received %c", square, expected, actual)
		}
	}
}

func TestSquareLeft(t *testing.T) {
	t.Parallel()
	cases := map[Square]Square{
		A1: None, A2: None, A3: None, A4: None, A5: None, A6: None, A7: None, A8: None,
		B1: A1, B2: A2, B3: A3, B4: A4, B5: A5, B6: A6, B7: A7, B8: A8,
		C1: B1, C2: B2, C3: B3, C4: B4, C5: B5, C6: B6, C7: B7, C8: B8,
		D1: C1, D2: C2, D3: C3, D4: C4, D5: C5, D6: C6, D7: C7, D8: C8,
		E1: D1, E2: D2, E3: D3, E4: D4, E5: D5, E6: D6, E7: D7, E8: D8,
		F1: E1, F2: E2, F3: E3, F4: E4, F5: E5, F6: E6, F7: E7, F8: E8,
		G1: F1, G2: F2, G3: F3, G4: F4, G5: F5, G6: F6, G7: F7, G8: F8,
		H1: G1, H2: G2, H3: G3, H4: G4, H5: G5, H6: G6, H7: G7, H8: G8,
		None: None,
	}
	for square, expected := range cases {
		if actual := square.Left(); actual != expected {
			t.Errorf("down for %s: expected %c; received %c", square, expected, actual)
		}
	}
}

func TestSquareRight(t *testing.T) {
	t.Parallel()
	cases := map[Square]Square{
		A1: B1, A2: B2, A3: B3, A4: B4, A5: B5, A6: B6, A7: B7, A8: B8,
		B1: C1, B2: C2, B3: C3, B4: C4, B5: C5, B6: C6, B7: C7, B8: C8,
		C1: D1, C2: D2, C3: D3, C4: D4, C5: D5, C6: D6, C7: D7, C8: D8,
		D1: E1, D2: E2, D3: E3, D4: E4, D5: E5, D6: E6, D7: E7, D8: E8,
		E1: F1, E2: F2, E3: F3, E4: F4, E5: F5, E6: F6, E7: F7, E8: F8,
		F1: G1, F2: G2, F3: G3, F4: G4, F5: G5, F6: G6, F7: G7, F8: G8,
		G1: H1, G2: H2, G3: H3, G4: H4, G5: H5, G6: H6, G7: H7, G8: H8,
		H1: None, H2: None, H3: None, H4: None, H5: None, H6: None, H7: None, H8: None,
		None: None,
	}
	for square, expected := range cases {
		if actual := square.Right(); actual != expected {
			t.Errorf("down for %s: expected %c; received %c", square, expected, actual)
		}
	}
}

func TestSquareAdd(t *testing.T) {
	t.Parallel()
	cases := []struct {
		start    Square
		movement Offset
		expected Square
	}{
		{A1, Offset{0, 0}, A1},
		{A1, Offset{2, 0}, C1},
		{A1, Offset{2, 2}, C3},
		{A1, Offset{0, 2}, A3},
		{A1, Offset{-2, 2}, None},
		{A1, Offset{-2, 0}, None},
		{A1, Offset{-2, -2}, None},
		{A1, Offset{0, -2}, None},
		{A1, Offset{2, -2}, None},
		{D4, Offset{0, 0}, D4},
		{D4, Offset{2, 0}, F4},
		{D4, Offset{2, 2}, F6},
		{D4, Offset{0, 2}, D6},
		{D4, Offset{-2, 2}, B6},
		{D4, Offset{-2, 0}, B4},
		{D4, Offset{-2, -2}, B2},
		{D4, Offset{0, -2}, D2},
		{D4, Offset{2, -2}, F2},
		{H8, Offset{0, 0}, H8},
		{H8, Offset{2, 0}, None},
		{H8, Offset{2, 2}, None},
		{H8, Offset{0, 2}, None},
		{H8, Offset{-2, 2}, None},
		{H8, Offset{-2, 0}, F8},
		{H8, Offset{-2, -2}, F6},
		{H8, Offset{0, -2}, H6},
		{H8, Offset{2, -2}, None},
	}
	for _, c := range cases {
		if actual := c.start.Add(c.movement); actual != c.expected {
			t.Errorf("%s add %v: expected %v; received %v", c.start, c.movement, c.expected, actual)
		}
	}
}

func TestSquareSub(t *testing.T) {
	t.Parallel()
	// From d4
	cases := map[Square]Offset{
		A1: {3, 3}, A2: {3, 2}, A3: {3, 1}, A4: {3, 0}, A5: {3, -1}, A6: {3, -2}, A7: {3, -3}, A8: {3, -4},
		B1: {2, 3}, B2: {2, 2}, B3: {2, 1}, B4: {2, 0}, B5: {2, -1}, B6: {2, -2}, B7: {2, -3}, B8: {2, -4},
		C1: {1, 3}, C2: {1, 2}, C3: {1, 1}, C4: {1, 0}, C5: {1, -1}, C6: {1, -2}, C7: {1, -3}, C8: {1, -4},
		D1: {0, 3}, D2: {0, 2}, D3: {0, 1}, D4: {0, 0}, D5: {0, -1}, D6: {0, -2}, D7: {0, -3}, D8: {0, -4},
		E1: {-1, 3}, E2: {-1, 2}, E3: {-1, 1}, E4: {-1, 0}, E5: {-1, -1}, E6: {-1, -2}, E7: {-1, -3}, E8: {-1, -4},
		F1: {-2, 3}, F2: {-2, 2}, F3: {-2, 1}, F4: {-2, 0}, F5: {-2, -1}, F6: {-2, -2}, F7: {-2, -3}, F8: {-2, -4},
		G1: {-3, 3}, G2: {-3, 2}, G3: {-3, 1}, G4: {-3, 0}, G5: {-3, -1}, G6: {-3, -2}, G7: {-3, -3}, G8: {-3, -4},
		H1: {-4, 3}, H2: {-4, 2}, H3: {-4, 1}, H4: {-4, 0}, H5: {-4, -1}, H6: {-4, -2}, H7: {-4, -3}, H8: {-4, -4},
		None: {0, 0},
	}
	for square, expected := range cases {
		if actual := D4.Sub(square); actual.Files != expected.Files || actual.Ranks != expected.Ranks {
			t.Errorf("D4 sub %s: expected %v; received %v", square, expected, actual)
		}
	}
}

func TestSquareForward(t *testing.T) {
	t.Parallel()
	cases := map[Piece]map[Square]Square{
		WhitePawn: {
			A1: A2, A2: A3, A3: A4, A4: A5, A5: A6, A6: A7, A7: A8, A8: None,
			B1: B2, B2: B3, B3: B4, B4: B5, B5: B6, B6: B7, B7: B8, B8: None,
			C1: C2, C2: C3, C3: C4, C4: C5, C5: C6, C6: C7, C7: C8, C8: None,
			D1: D2, D2: D3, D3: D4, D4: D5, D5: D6, D6: D7, D7: D8, D8: None,
			E1: E2, E2: E3, E3: E4, E4: E5, E5: E6, E6: E7, E7: E8, E8: None,
			F1: F2, F2: F3, F3: F4, F4: F5, F5: F6, F6: F7, F7: F8, F8: None,
			G1: G2, G2: G3, G3: G4, G4: G5, G5: G6, G6: G7, G7: G8, G8: None,
			H1: H2, H2: H3, H3: H4, H4: H5, H5: H6, H6: H7, H7: H8, H8: None,
			None: None,
		},
		BlackPawn: {
			A1: None, A2: A1, A3: A2, A4: A3, A5: A4, A6: A5, A7: A6, A8: A7,
			B1: None, B2: B1, B3: B2, B4: B3, B5: B4, B6: B5, B7: B6, B8: B7,
			C1: None, C2: C1, C3: C2, C4: C3, C5: C4, C6: C5, C7: C6, C8: C7,
			D1: None, D2: D1, D3: D2, D4: D3, D5: D4, D6: D5, D7: D6, D8: D7,
			E1: None, E2: E1, E3: E2, E4: E3, E5: E4, E6: E5, E7: E6, E8: E7,
			F1: None, F2: F1, F3: F2, F4: F3, F5: F4, F6: F5, F7: F6, F8: F7,
			G1: None, G2: G1, G3: G2, G4: G3, G5: G4, G6: G5, G7: G6, G8: G7,
			H1: None, H2: H1, H3: H2, H4: H3, H5: H4, H6: H5, H7: H6, H8: H7,
			None: None,
		},
		Empty: {
			A1: A1, A2: A2, A3: A3, A4: A4, A5: A5, A6: A6, A7: A7, A8: A8,
			B1: B1, B2: B2, B3: B3, B4: B4, B5: B5, B6: B6, B7: B7, B8: B8,
			C1: C1, C2: C2, C3: C3, C4: C4, C5: C5, C6: C6, C7: C7, C8: C8,
			D1: D1, D2: D2, D3: D3, D4: D4, D5: D5, D6: D6, D7: D7, D8: D8,
			E1: E1, E2: E2, E3: E3, E4: E4, E5: E5, E6: E6, E7: E7, E8: E8,
			F1: F1, F2: F2, F3: F3, F4: F4, F5: F5, F6: F6, F7: F7, F8: F8,
			G1: G1, G2: G2, G3: G3, G4: G4, G5: G5, G6: G6, G7: G7, G8: G8,
			H1: H1, H2: H2, H3: H3, H4: H4, H5: H5, H6: H6, H7: H7, H8: H8,
			None: None,
		},
	}
	for piece, subcases := range cases {
		for square, expected := range subcases {
			if actual := square.Forward(piece); actual != expected {
				t.Errorf("forward from %s for %c: expected %c; received %c", square, piece.Rune(), expected, actual)
			}
		}
	}
}

func TestSquareString(t *testing.T) {
	t.Parallel()
	cases := map[Square]string{
		A1: "a1", A2: "a2", A3: "a3", A4: "a4", A5: "a5", A6: "a6", A7: "a7", A8: "a8",
		B1: "b1", B2: "b2", B3: "b3", B4: "b4", B5: "b5", B6: "b6", B7: "b7", B8: "b8",
		C1: "c1", C2: "c2", C3: "c3", C4: "c4", C5: "c5", C6: "c6", C7: "c7", C8: "c8",
		D1: "d1", D2: "d2", D3: "d3", D4: "d4", D5: "d5", D6: "d6", D7: "d7", D8: "d8",
		E1: "e1", E2: "e2", E3: "e3", E4: "e4", E5: "e5", E6: "e6", E7: "e7", E8: "e8",
		F1: "f1", F2: "f2", F3: "f3", F4: "f4", F5: "f5", F6: "f6", F7: "f7", F8: "f8",
		G1: "g1", G2: "g2", G3: "g3", G4: "g4", G5: "g5", G6: "g6", G7: "g7", G8: "g8",
		H1: "h1", H2: "h2", H3: "h3", H4: "h4", H5: "h5", H6: "h6", H7: "h7", H8: "h8",
		None: "??",
	}
	for square, expected := range cases {
		if actual := square.String(); actual != expected {
			t.Errorf("string for %s: expected %s; received %s", square, expected, actual)
		}
	}
}

func BenchmarkSquareAdd(b *testing.B) {
	d := Offset{7, 7}
	for i := 0; i < b.N; i++ {
		A1.Add(d)
	}
}

func BenchmarkSquareSub(b *testing.B) {
	for i := 0; i < b.N; i++ {
		D4.Sub(Square(i % 63))
	}
}

func BenchmarkSquareUnit(b *testing.B) {
	d := Offset{-8, -8}
	for i := 0; i < b.N; i++ {
		d.Unit()
	}
}
