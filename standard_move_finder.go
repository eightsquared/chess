package chess

// StandardMoveFinder provides a generally readable move detection algorithm
// that still performs reasonably well, though not quite as well as the
// optimized bitwise algorithm.
type StandardMoveFinder struct {
}

var (
	knightMoves = [8]Offset{
		{2, 1},
		{1, 2},
		{-1, 2},
		{-2, 1},
		{-2, -1},
		{-1, -2},
		{1, -2},
		{2, -1},
	}
	bishopMoves = [4]Offset{
		{1, 1},
		{-1, 1},
		{-1, -1},
		{1, -1},
	}
	rookMoves = [4]Offset{
		{1, 0},
		{0, 1},
		{-1, 0},
		{0, -1},
	}
	queenMoves = [8]Offset{
		{1, 0},
		{1, 1},
		{0, 1},
		{-1, 1},
		{-1, 0},
		{-1, -1},
		{0, -1},
		{1, -1},
	}
)

// Moves determines the set of valid moves ms according to the current state of
// the given Game g.
func (smf *StandardMoveFinder) Moves(g Game) (ms Moves) {
	b := g.Board

	// Assign the color with the move to att(acking), and the opponent's color to
	// def(ending). Also locate the attacking king's square ahead of time, as we
	// will need this to eliminate moves that leave the king in check.
	att, def := g.ToMove, g.ToMove.Invert()
	ks := b.Find(att|King, None)

	// For pawn moves, establish the starting rank, the rank at which an en
	// passant capture may be possible, and the rank immediately preceding the
	// final rank (on which a pawn is promoted). Assigning these values to
	// variables will make the pawn portion of the algorithm more readable.
	var firstRank, enPassantRank, promotionRank rune
	if att.Is(White) {
		firstRank, enPassantRank, promotionRank = '2', '5', '7'
	} else if att.Is(Black) {
		firstRank, enPassantRank, promotionRank = '7', '4', '2'
	}

	// checkAndAppend appends the given move m to ms if and only if applying m to
	// the current board does not result in the given square s being attacked by
	// the defending color. The return value indicates whether the move was
	// appended.
	checkAndAppend := func(m Move, s Square) (ok bool) {
		b2 := b.Apply(m)
		if s != None && smf.Attacked(b2, s, def) {
			return
		}
		ms, ok = append(ms, m), true
		return
	}

	// Loop through all attacking pieces on the board.
	for s := b.Find(att, None); s != None; s = b.Find(att, s) {
		p := b[s]
		switch {
		case p.Is(Pawn):
			// Capture the pawn's current rank, as we need it to determine which
			// moves are available.
			r := s.Rank()
			if r == promotionRank {
				// On the rank prior to the last rank, the pawn has three options:
				// advance one square (if that square is empty), capture to the left,
				// or capture to the right. For each option, we check one move for
				// validity and then add the remaining three promotion options without
				// taking the performance hit of a move check.
				if a := s.Forward(p); b[a] == Empty {
					if checkAndAppend(Move{s, a, ReplaceWithKnight, a}, ks) {
						ms = append(ms, Move{s, a, ReplaceWithBishop, a})
						ms = append(ms, Move{s, a, ReplaceWithRook, a})
						ms = append(ms, Move{s, a, ReplaceWithQueen, a})
					}
				}
				if a := s.Forward(p).Left(); a != None && b[a].Is(def) {
					if checkAndAppend(Move{s, a, ReplaceWithKnight, a}, ks) {
						ms = append(ms, Move{s, a, ReplaceWithBishop, a})
						ms = append(ms, Move{s, a, ReplaceWithRook, a})
						ms = append(ms, Move{s, a, ReplaceWithQueen, a})
					}
				}
				if a := s.Forward(p).Right(); a != None && b[a].Is(def) {
					if checkAndAppend(Move{s, a, ReplaceWithKnight, a}, ks) {
						ms = append(ms, Move{s, a, ReplaceWithBishop, a})
						ms = append(ms, Move{s, a, ReplaceWithRook, a})
						ms = append(ms, Move{s, a, ReplaceWithQueen, a})
					}
				}
			} else {
				// Otherwise the pawn may either advance or capture.
				if a := s.Forward(p); a != None && b[a] == Empty {
					checkAndAppend(Move{s, a}, ks)
					// If we can advance one square and we are on the starting rank, see
					// whether we can advance a second square.
					if a = a.Forward(p); r == firstRank && b[a] == Empty {
						checkAndAppend(Move{s, a}, ks)
					}
				}
				// Check for potential captures to the left and right, including the
				// possibility of capturing en passant if the position's en passant
				// target square is one square forward and one square to the left or
				// right.
				if a := s.Forward(p).Left(); a != None {
					if b[a].Is(def) {
						checkAndAppend(Move{s, a}, ks)
					}
					if r == enPassantRank && a == g.EnPassant {
						checkAndAppend(Move{s, a, s.Left(), None}, ks)
					}
				}
				if a := s.Forward(p).Right(); a != None {
					if b[a].Is(def) {
						checkAndAppend(Move{s, a}, ks)
					}
					if r == enPassantRank && a == g.EnPassant {
						checkAndAppend(Move{s, a, s.Right(), None}, ks)
					}
				}
			}
		case p.Is(Knight):
			// A knight may move to any one of its eight offsets if that square is
			// empty or if it is occupied by a defending piece.
			for _, o := range knightMoves {
				if a := s.Add(o); a != None && (b[a] == Empty || b[a].Is(def)) {
					checkAndAppend(Move{s, a}, ks)
				}
			}
		case p.Is(Bishop):
			// A bishop may move along any diagonal until encountering the edge of
			// the board or a non-empty square.
			for _, o := range bishopMoves {
				for a := s.Add(o); a != None; a = a.Add(o) {
					if b[a] == Empty {
						checkAndAppend(Move{s, a}, ks)
						continue
					}
					if b[a].Is(def) {
						checkAndAppend(Move{s, a}, ks)
					}
					break
				}
			}
		case p.Is(Rook):
			// A rook may move along any file or rank until encountering the edge of
			// the board or a non-empty square.
			for _, o := range rookMoves {
				for a := s.Add(o); a != None; a = a.Add(o) {
					if b[a] == Empty {
						checkAndAppend(Move{s, a}, ks)
						continue
					}
					if b[a].Is(def) {
						checkAndAppend(Move{s, a}, ks)
					}
					break
				}
			}
		case p.Is(Queen):
			// A queen may move along any diagonal, file, or rank until encountering
			// the edge of the board or a non-empty square.
			for _, o := range queenMoves {
				for a := s.Add(o); a != None; a = a.Add(o) {
					if b[a] == Empty {
						checkAndAppend(Move{s, a}, ks)
						continue
					}
					if b[a].Is(def) {
						checkAndAppend(Move{s, a}, ks)
					}
					break
				}
			}
		case p.Is(King):
			// A king may move in the same direction as a queen, but no more than one
			// square.
			for _, o := range queenMoves {
				if a := s.Add(o); a != None && (b[a] == Empty || b[a].Is(def)) {
					checkAndAppend(Move{s, a}, a)
				}
			}
			if att.Is(White) && s == E1 {
				// A white king on e1 may castle kingside if a) the position allows it,
				// b) both f1 and g1 are empty, c) h1 contains a white rook, and d) it
				// is not currently attacked.
				if g.Castling.Allows(WhiteKingside) &&
					b[F1] == Empty && b[G1] == Empty &&
					b[H1] == WhiteRook &&
					!smf.Attacked(b, E1, def) {
					checkAndAppend(Move{s, G1, H1, F1}, G1)
				}
				// A white king on e1 may castle queenside if a) the position allows
				// it, b) d1, c1, and b1 are empty, c) a1 contains a white rook, d) it
				// is not currently attacked, and e) d1 is not currently attacked.
				if g.Castling.Allows(WhiteQueenside) &&
					b[D1] == Empty && b[C1] == Empty && b[B1] == Empty &&
					b[A1] == WhiteRook &&
					!smf.Attacked(b, E1, def) &&
					!smf.Attacked(b, D1, def) {
					checkAndAppend(Move{s, C1, A1, D1}, C1)
				}
			}
			if att.Is(Black) && s == E8 {
				// A black king on e8 may castle kingside if a) the position allows it,
				// b) both f8 and g8 are empty, c) h8 contains a white rook, and d) it
				// is not currently attacked.
				if g.Castling.Allows(BlackKingside) &&
					b[F8] == Empty && b[G8] == Empty &&
					b[H8] == BlackRook &&
					!smf.Attacked(b, E8, def) {
					checkAndAppend(Move{s, G8, H8, F8}, G8)
				}
				// A black king on e8 may castle queenside if a) the position allows
				// it, b) d8, c8, and b8 are empty, c) a8 contains a white rook, d) it
				// is not currently attacked, and e) d8 is not currently attacked.
				if g.Castling.Allows(BlackQueenside) &&
					b[D8] == Empty && b[C8] == Empty && b[B8] == Empty &&
					b[A8] == BlackRook &&
					!smf.Attacked(b, E8, def) &&
					!smf.Attacked(b, D8, def) {
					checkAndAppend(Move{s, C8, A8, D8}, C8)
				}
			}
		}
	}
	return
}

// Attacked returns true if the given square s in the given board b is attacked
// by any piece of the same color as the given piece p.
func (*StandardMoveFinder) Attacked(b Board, s Square, p Piece) (attacked bool) {
	if !s.Valid() {
		return false
	}
	// Loop through all departure squares d in the board that contain a piece
	// matching the color c.
	c := p.Color()
	for d := b.Find(c, None); d != None; d = b.Find(c, d) {
		// A piece does not attack its own square.
		if d == s {
			continue
		}
		// p holds the piece on square d, while a and u will hold the absolute
		// offset and the unit offset from d to s.
		p := b[d]
		a, u := s.Sub(d).Abs(), s.Sub(d).Unit()
		switch {
		case p.Is(Pawn):
			// A pawn attacks s if s is one square forward and one square to either
			// the left or right.
			if d.Forward(p).Left() == s || d.Forward(p).Right() == s {
				attacked = true
				return
			}
		case p.Is(Knight):
			// A knight attacks s if s is offset from d by exactly one file and two
			// ranks, or two files and one rank.
			if (a.Files == 1 && a.Ranks == 2) || (a.Files == 2 && a.Ranks == 1) {
				attacked = true
				return
			}
		case p.Is(King):
			// A king attacks s if s is no more than one file or rank from d.
			if a.Files <= 1 && a.Ranks <= 1 {
				attacked = true
				return
			}
		case a.Files == a.Ranks && (p.Is(Bishop) || p.Is(Queen)):
			// If the absolute file offset equals the absolute rank offset, we know
			// that d is on a diagonal shared by s. A bishop or queen on d attacks s
			// if we can step from d to s by successively adding the unit offset,
			// stopping if we reach a non-empty square.
			for t := d.Add(u); t != None; t = t.Add(u) {
				if t == s {
					attacked = true
					return
				}
				if b[t] != Empty {
					break
				}
			}
		case (u.Files == 0 || u.Ranks == 0) && (p.Is(Rook) || p.Is(Queen)):
			// If either the absolute file offset or the absolute rank offset is
			// zero, we know that d is on a file or a rank shared by s. A rook or
			// queen on d attacks s if we can step from d to s by successively adding
			// the unit offset, stopping if we reach a non-empty square.
			for t := d.Add(u); t != None; t = t.Add(u) {
				if t == s {
					attacked = true
					return
				}
				if b[t] != Empty {
					break
				}
			}
		}
	}
	return
}

func (*StandardMoveFinder) String() string {
	return "standard"
}
